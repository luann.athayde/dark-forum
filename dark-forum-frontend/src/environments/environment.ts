/**
 * Default enviroment file
 */
export const environment = {
    production: false,
    URL_API: 'http://localhost:8080/dark-forum/api',
    URL_AUTH_API: 'http://localhost:8080/dark-forum/api',
    USE_RAGNAROK_LOGIN: true,
    CATEGORY_GROUP_ID: [1],
    FORUM_GROUP_ID: [1, 2],
    TOPIC_GROUP_ID: [1, 2, 3]
};
