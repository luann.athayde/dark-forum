import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForumAuthGuard } from './shared/authguard/forum.auth-guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./modules/homepage/homepage.module').then(m => m.HomepageModule),
        canActivate: [ForumAuthGuard],
        canActivateChild: [ForumAuthGuard]
    },
    {
        path: 'categories',
        loadChildren: () => import('./modules/categories/categories.module').then(m => m.CategoriesModule),
        canActivate: [ForumAuthGuard],
        canActivateChild: [ForumAuthGuard],
        data: {id: 'categories', icon: 'pi pi-folder'}
    },
    {
        path: 'forums',
        loadChildren: () => import('./modules/forums/forums.module').then(m => m.ForumsModule),
        canActivate: [ForumAuthGuard],
        canActivateChild: [ForumAuthGuard],
        data: {id: 'forums', icon: 'pi pi-bars'}
    },
    {
        path: 'topics',
        loadChildren: () => import('./modules/topics/topics.module').then(m => m.TopicsModule),
        canActivate: [ForumAuthGuard],
        canActivateChild: [ForumAuthGuard],
        data: {id: 'topics', icon: 'pi pi-file'}
    },
    {
        path: 'tags',
        loadChildren: () => import('./modules/tags/tags.module').then(m => m.TagsModule),
        canActivate: [ForumAuthGuard],
        canActivateChild: [ForumAuthGuard],
        data: {id: 'tags', icon: 'pi pi-tag'}
    },
    {
        path: 'users',
        loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule),
        canActivate: [ForumAuthGuard],
        canActivateChild: [ForumAuthGuard],
        data: {id: 'profile', icon: 'pi pi-user'}
    },
    {
        path: 'admin',
        loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule),
        canActivate: [ForumAuthGuard],
        canActivateChild: [ForumAuthGuard],
        data: {id: 'admin', icon: 'pi pi-briefcase'}
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: '/'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
