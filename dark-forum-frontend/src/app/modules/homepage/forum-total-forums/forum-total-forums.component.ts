import { Component, OnInit } from '@angular/core';
import { AppUtil } from '../../../shared/util/app.util';

@Component({
    selector: 'app-forum-total-forums',
    templateUrl: './forum-total-forums.component.html',
    styleUrls: ['./forum-total-forums.component.scss']
})
export class ForumTotalForumsComponent implements OnInit {

    data: any;

    ngOnInit(): void {
        this.data = {
            labels: ['Abr', 'Mai', 'Jun', 'Jul', 'Ago'],
            datasets: [
                {
                    label: 'Novos',
                    data: [...AppUtil.getInstance().getRandomData(2)]
                },
                {
                    label: 'Respondidos',
                    data: [...AppUtil.getInstance().getRandomData(2)]
                }
            ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        };
    }

}
