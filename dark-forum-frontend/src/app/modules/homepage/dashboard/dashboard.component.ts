import { Component, OnInit } from '@angular/core';
import { Language } from '../../../shared/locale/language';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { AppUtil } from '../../../shared/util/app.util';
import { User } from '../../../shared/model/user.model';
import { UserService } from '../../../shared/service/user.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    loggedUser = User.getInstance();

    util: AppUtil = AppUtil.getInstance();
    lang: Language = LanguageManager.getInstance().getLanguage();

    onlineUsers: User[] = [];

    constructor(private userService: UserService) {
    }

    ngOnInit(): void {

        this.userService.listOnlineUsers().subscribe(users => {
            this.onlineUsers = [...users];
        });

    }

}
