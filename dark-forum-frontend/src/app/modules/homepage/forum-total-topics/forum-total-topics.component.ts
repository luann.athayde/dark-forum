import { Component, OnInit } from '@angular/core';
import { AppUtil } from '../../../shared/util/app.util';

@Component({
    selector: 'app-forum-total-topics',
    templateUrl: './forum-total-topics.component.html',
    styleUrls: ['./forum-total-topics.component.scss']
})
export class ForumTotalTopicsComponent implements OnInit {

    data: any;

    ngOnInit(): void {
        this.data = {
            labels: ['Abr', 'Mai', 'Jun', 'Jul', 'Ago'],
            datasets: [
                {
                    label: 'Visualizações',
                    data: [...AppUtil.getInstance().getRandomData(2)]
                },
                {
                    label: 'Respostas',
                    data: [...AppUtil.getInstance().getRandomData(2)]
                }
            ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        };
    }

}
