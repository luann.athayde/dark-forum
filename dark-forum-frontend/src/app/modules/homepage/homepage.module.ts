import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomepageRoutingModule } from './homepage-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoriesModule } from '../categories/categories.module';
import { TopicsModule } from '../topics/topics.module';
import { EventsModule } from '../events/events.module';
import { ForumOnlineUsersComponent } from './forum-online-users/forum-online-users.component';
import { ForumTotalTopicsComponent } from './forum-total-topics/forum-total-topics.component';
import { ForumTotalForumsComponent } from './forum-total-forums/forum-total-forums.component';
import { ChartModule } from 'primeng/chart';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';

@NgModule({
    declarations: [DashboardComponent, ForumOnlineUsersComponent, ForumTotalTopicsComponent, ForumTotalForumsComponent],
    imports: [
        CommonModule,
        HomepageRoutingModule,
        CategoriesModule,
        TopicsModule,
        EventsModule,
        ChartModule,
        CardModule,
        ButtonModule
    ]
})
export class HomepageModule {
}
