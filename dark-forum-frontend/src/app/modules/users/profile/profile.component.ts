import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User } from '../../../shared/model/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AppUtil } from '../../../shared/util/app.util';
import { Language } from '../../../shared/locale/language';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { UserService } from '../../../shared/service/user.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {

    lang: Language = LanguageManager.getInstance().getLanguage();
    util: AppUtil = AppUtil.getInstance();
    // user: User = User.getInstance();
    user: User = new User();
    loggedUser: User = User.getInstance();

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private userService: UserService) {
    }

    ngOnInit() {
        this.activatedRoute.paramMap.subscribe(params => {
            const userId = +params.get('id');
            if ( userId && userId > 0 ) {
                // ---
                this.util.blockUI();
                this.userService.findById(userId).subscribe(user => {
                    this.user.set(user);
                });
                // ---
            } else if (this.loggedUser && this.loggedUser.id > 0) {
                Object.assign(this.user, this.loggedUser);
            } else {
                this.router.navigate(['/']).then();
            }
        });
    }

}
