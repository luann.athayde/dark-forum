import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { CardModule } from 'primeng/card';
import { TabViewModule } from 'primeng/tabview';

@NgModule({
    declarations: [LoginComponent, ProfileComponent],
    imports: [
        CommonModule,
        UsersRoutingModule,
        ProgressSpinnerModule,
        CardModule,
        TabViewModule
    ]
})
export class UsersModule {
}
