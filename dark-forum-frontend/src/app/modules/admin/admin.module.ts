import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { CategoriesModule } from '../categories/categories.module';
import { CardModule } from 'primeng/card';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';
import { ForumsModule } from '../forums/forums.module';
import { TooltipModule } from 'primeng/tooltip';
import { DataViewModule } from 'primeng/dataview';

@NgModule({
    declarations: [AdminHomeComponent],
    imports: [
        CommonModule,
        AdminRoutingModule,
        CategoriesModule,
        CardModule,
        ProgressSpinnerModule,
        ButtonModule,
        AccordionModule,
        ForumsModule,
        TooltipModule,
        DataViewModule
    ]
})
export class AdminModule {
}
