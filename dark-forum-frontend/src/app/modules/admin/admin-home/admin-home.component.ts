import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CategoryService } from '../../../shared/service/category.service';
import { Category } from '../../../shared/model/category.model';
import { Language } from '../../../shared/locale/language';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { AppUtil } from '../../../shared/util/app.util';
import { User } from '../../../shared/model/user.model';
import { Router } from '@angular/router';
import { ToasterService } from '../../../shared/service/toaster.service';
import { ConfirmationService } from 'primeng/api';
import { CreateCategoryComponent } from '../../categories/create-category/create-category.component';

@Component({
    selector: 'app-admin-home',
    templateUrl: './admin-home.component.html',
    styleUrls: ['./admin-home.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AdminHomeComponent implements OnInit {

    lang: Language = LanguageManager.getInstance().getLanguage();
    util: AppUtil = AppUtil.getInstance();
    user: User = User.getInstance();

    @ViewChild('createCategory', {static: true})
    createCategoryDialog: CreateCategoryComponent;

    loading = true;

    categories: Category[] = [];

    constructor(private router: Router,
                private toast: ToasterService,
                private confirmationService: ConfirmationService,
                private categoryService: CategoryService) {
    }

    ngOnInit(): void {
        this.loadCategories();
    }

    loadCategories() {
        this.loading = true;
        this.categoryService.listCategories({first: 0, rows: 2048}).subscribe(data => {
            this.categories = [...data.list];
            this.loading = false;
        });
    }

    onCategoryCreated() {
        this.loadCategories();
    }

    editCategory(category: Category) {
        this.createCategoryDialog.category = new Category();
        Object.assign(this.createCategoryDialog.category, category);
        this.createCategoryDialog.showCreateCategoryModal();
    }

    deleteCategory(category: Category) {
        this.confirmationService.confirm({
            message: this.lang.getDeleteCategoryInfo()
                .replace('{name}', category.name)
                .replace('{slug}', category.slug),
            accept: () => {
                this.util.blockUI();
                this.categoryService.delete(this.user, category).subscribe(() => {
                    this.router.navigate(['/']).then(() => {
                        this.util.unblockUI();
                    });
                }, () => {
                    this.router.navigate(['/']).then(() => {
                        this.util.unblockUI();
                        this.toast.warn('Fail to delete category! Try again later.');
                    });
                });
            },
            acceptIcon: 'pi pi-check',
            acceptLabel: this.lang.getConfirm(),
            rejectIcon: 'pi pi-times',
            rejectLabel: this.lang.getCancel()
        });
    }

}
