import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Topic } from '../../../shared/model/topic.model';
import { DomSanitizer } from '@angular/platform-browser';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { Language } from '../../../shared/locale/language';
import { TopicService } from '../../../shared/service/topic.service';
import { AppUtil } from '../../../shared/util/app.util';
import { User } from '../../../shared/model/user.model';
import { Reply } from '../../../shared/model/reply.model';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-topic',
    templateUrl: './topic.component.html',
    styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {

    lang: Language = LanguageManager.getInstance().getLanguage();
    util: AppUtil = AppUtil.getInstance();
    user: User = User.getInstance();

    @Input() topic: Topic = new Topic();
    reply: Reply = new Reply();
    loadingTopic = true;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private sanitizer: DomSanitizer,
                private topicService: TopicService) {
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(params => {
            const slug = params.get('slug');
            if (slug) {
                // ---
                this.loadingTopic = true;
                this.topicService.findBySlug(slug).subscribe(topic => {
                    Object.assign(this.topic, topic);
                    this.loadingTopic = false;
                }, () => {
                    this.router.navigate(['/']).then(() => {
                        this.loadingTopic = false;
                    });
                });
            } else {
                this.router.navigate(['/']).then();
            }
        });
    }

    trustHtml(value: string) {
        try {
            return this.sanitizer.bypassSecurityTrustHtml(value);
        } catch (e) {
            AppUtil.handleErros(e);
        }
        return '';
    }

    trustImage(value: string) {
        try {
            return this.sanitizer.bypassSecurityTrustUrl(value);
        } catch (e) {
            AppUtil.handleErros(e);
        }
        return '';
    }

    canEditTopic(): boolean {
        const index = environment.FORUM_GROUP_ID.findIndex(id => {
            return this.user.group.id === id;
        });
        return !this.util.blockedUI && (index >= 0 || this.topic.user.id === this.user.id);
    }

    editTopic() {
        this.router.navigate(['/topics/topic/edit', this.topic.slug]).then();
    }

}
