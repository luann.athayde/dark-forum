import { Component, OnInit } from '@angular/core';
import { Topic } from '../../../shared/model/topic.model';

@Component({
    selector: 'app-list-topics',
    templateUrl: './list-topics.component.html',
    styleUrls: ['./list-topics.component.scss']
})
export class ListTopicsComponent implements OnInit {

    topics: Topic[] = [];

    constructor() {
    }

    ngOnInit(): void {
        for (let i = 0; i < 4; i++) {
            this.topics.push({
                createDate: new Date(),
                name: 'Topico ' + i,
                slug: 'topico-' + i,
                id: i + 1
            });
        }
    }

    changePage(event: any) {

    }

}
