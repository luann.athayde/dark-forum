import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopicsRoutingModule } from './topics-routing.module';
import { TopicComponent } from './topic/topic.component';
import { FormsModule } from '@angular/forms';
import { ListTopicsComponent } from './list-topics/list-topics.component';
import { SmallTopicComponent } from './small-topic/small-topic.component';
import { CreateTopicComponent } from './create-topic/create-topic.component';
import { TagsModule } from '../tags/tags.module';
import { TooltipModule } from 'primeng/tooltip';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import { InputTextModule } from 'primeng/inputtext';
import { EditorModule } from 'primeng/editor';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ChipsModule } from 'primeng/chips';
import { CardModule } from 'primeng/card';
import { RatingModule } from 'primeng/rating';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

@NgModule({
    declarations: [
        TopicComponent,
        ListTopicsComponent,
        SmallTopicComponent,
        CreateTopicComponent
    ],
    exports: [
        ListTopicsComponent,
        SmallTopicComponent,
        CreateTopicComponent
    ],
    imports: [
        CommonModule,
        TopicsRoutingModule,
        FormsModule,
        TagsModule,
        TooltipModule,
        ButtonModule,
        PaginatorModule,
        InputTextModule,
        EditorModule,
        ToggleButtonModule,
        ChipsModule,
        CardModule,
        RatingModule,
        ProgressSpinnerModule
    ]
})
export class TopicsModule {
}
