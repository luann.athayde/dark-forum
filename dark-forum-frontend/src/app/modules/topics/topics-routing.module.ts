import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopicComponent } from './topic/topic.component';
import { CreateTopicComponent } from './create-topic/create-topic.component';

/**
 * Topic Routes
 */
const routes: Routes = [
    {
        path: 'topic/:slug',
        component: TopicComponent
    },
    {
        path: 'topic/:type/:slug',
        component: CreateTopicComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TopicsRoutingModule { }
