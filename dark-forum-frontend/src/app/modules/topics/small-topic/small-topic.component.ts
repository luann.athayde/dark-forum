import { Component, OnInit } from '@angular/core';
import { Language } from '../../../shared/locale/language';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { Topic } from '../../../shared/model/topic.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { TopicService } from '../../../shared/service/topic.service';
import { AppUtil } from '../../../shared/util/app.util';

@Component({
    selector: 'app-small-topic',
    templateUrl: './small-topic.component.html',
    styleUrls: ['./small-topic.component.scss']
})
export class SmallTopicComponent implements OnInit {

    lang: Language = LanguageManager.getInstance().getLanguage();

    topics: Topic [] = [];
    loadingTopics = true;
    showTopics = true;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private sanitizer: DomSanitizer,
                private topicService: TopicService) {
    }

    ngOnInit(): void {
        this.loadingTopics = true;
        this.topicService.listTopics({first: 0, rows: 5, hot: true}).subscribe(data => {
            this.topics = [...data.list];
            this.loadingTopics = false;
            this.showTopics = true;
        }, error => {
            AppUtil.handleErros(error);
            this.topics = [];
            this.loadingTopics = this.showTopics = false;
        });
    }

    trustImage(value: string) {
        try {
            return this.sanitizer.bypassSecurityTrustUrl(value);
        } catch (e) {
        }
        return '';
    }

}
