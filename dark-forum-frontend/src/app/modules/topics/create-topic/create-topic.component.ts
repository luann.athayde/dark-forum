import { AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { Language } from '../../../shared/locale/language';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { AppUtil } from '../../../shared/util/app.util';
import { ActivatedRoute, Router } from '@angular/router';

import { ToasterService } from '../../../shared/service/toaster.service';
import { ForumService } from '../../../shared/service/forum.service';
import { TopicService } from '../../../shared/service/topic.service';

import { User } from '../../../shared/model/user.model';
import { Forum } from '../../../shared/model/forum.model';
import { Topic } from '../../../shared/model/topic.model';
import { Editor } from 'primeng/editor';

@Component({
    selector: 'app-create-topic',
    templateUrl: './create-topic.component.html',
    styleUrls: ['./create-topic.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CreateTopicComponent implements OnInit, AfterViewInit {

    @ViewChild('topicEditor', {static: true})
    topicEditor: Editor;

    @Output()
    topicCreated: EventEmitter<Topic> = new EventEmitter<Topic>();

    lang: Language = LanguageManager.getInstance().getLanguage();
    util: AppUtil = AppUtil.getInstance();
    user: User = User.getInstance();

    modalState = false;
    forum: Forum = new Forum();
    topic: Topic;
    tags: string[] = ['DarkFireRO'];
    editorSize = 20;

    constructor(private toast: ToasterService,
                private forumService: ForumService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private topicService: TopicService) {
    }

    ngOnInit() {
        this.topic = new Topic();
        this.topic.user = this.user;
        this.activatedRoute.paramMap.subscribe(params => {
            const type = params.get('type');
            const slug = params.get('slug');
            if (slug && type === 'new') {
                // ---
                this.util.blockUI();
                this.forumService.findBySlug(slug).subscribe(forum => {
                    Object.assign(this.forum, forum);
                    this.topic.forum = forum;
                    this.util.unblockUI();
                }, (error) => {
                    this.router.navigate(['/']).then(() => {
                        this.util.unblockUI();
                        AppUtil.handleErros(error, this.toast);
                    });
                });
                // ---
            } else if (slug && type === 'edit') {
                // ---
                this.util.blockUI();
                this.topicService.findBySlug(slug).subscribe(topic => {
                    this.forum = topic.forum;
                    Object.assign(this.topic, topic);
                    this.tags = [];
                    this.topic.tags.forEach(topicTag => {
                        this.tags.push(topicTag.tag.name);
                    });
                    this.util.unblockUI();
                }, (error) => {
                    this.router.navigate(['/']).then(() => {
                        this.util.unblockUI();
                        AppUtil.handleErros(error, this.toast);
                    });
                });
                // ---
            }
        });
        // ---
        if (this.topicEditor) {
            console.log(this.topicEditor.getQuill());
        }
    }

    ngAfterViewInit() {
    }

    createSlug() {
        if (this.topic.name) {
            this.topic.slug = this.topic.name
                .toLowerCase()
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, '')
                .replace(/\s/g, '-')
                .substring(0, 48);
        }
    }

    saveTopic() {
        this.toast.info('O nome do tópico deve ser preenchido!');
        if (this.topicValid()) {
            this.util.blockUI();
            this.topicService.save(this.topic).subscribe(() => {
                this.util.unblockUI();
                this.router.navigate(['/forums/forum', this.forum.slug])
                    .then(() => {
                        this.toast.success('Topic created succesfully!');
                    });
            }, error => {
                this.util.unblockUI();
                AppUtil.handleErros(error, this.toast);
            });
        }
    }

    topicValid(): boolean {
        let valid = true;

        if (!this.topic.name || this.topic.name.length < 4 || this.topic.name.length > 256) {
            this.toast.warn('O nome do tópico deve ser preenchido!');
            valid = false;
        }

        return valid;
    }

    addTopicTag(event: any) {
        if (event && event.value) {
            this.topic.tags.push({
                tag: {
                    name: event.value,
                    user: {id: this.user.id}
                }
            });
        }
    }

    rmTopicTag(event: any) {
        if (event && event.value) {
            const index = this.topic.tags.findIndex(topicTag => {
                return topicTag.tag.name === event.value;
            });
            this.topic.tags.splice(index, 1);
        }
    }

    addSize(size: number) {
        this.editorSize += size;
        if (this.editorSize < 20) {
            this.editorSize = 20;
        }
        console.log('current size = ' + this.editorSize);
    }

    getEditorSize(): number {
        return this.editorSize;
    }

}
