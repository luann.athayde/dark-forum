import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Language } from '../../../shared/locale/language';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Category } from '../../../shared/model/category.model';
import { environment } from '../../../../environments/environment';
import { AppUtil } from '../../../shared/util/app.util';
import { User } from '../../../shared/model/user.model';
import { CategoryService } from '../../../shared/service/category.service';
import { ToasterService } from '../../../shared/service/toaster.service';
import { Forum } from '../../../shared/model/forum.model';
import { ConfirmationService } from 'primeng/api';
import { ForumService } from '../../../shared/service/forum.service';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CategoryComponent implements OnInit {

    lang: Language = LanguageManager.getInstance().getLanguage();
    util: AppUtil = AppUtil.getInstance();
    user: User = User.getInstance();

    @Input() category: Category = {name: 'Loading...'};
    @Input() toggleable = false;

    forums: Forum[] = [];
    total = 0;
    rows = 5;
    showContent = true;
    loadingCategory = true;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private sanitizer: DomSanitizer,
                private toast: ToasterService,
                private confirmationService: ConfirmationService,
                private categoryService: CategoryService,
                private forumService: ForumService) {
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(params => {
            const slug = params.get('slug');
            if (slug) {
                this.categoryService.findBySlug(slug).subscribe(category => {
                    this.category = category;
                    this.loadForums();
                }, error => {
                    AppUtil.handleErros(error, this.toast);
                    this.loadingCategory = false;
                });
            } else if (!this.category || !this.category.slug) {
                this.router.navigate(['']).then();
            }
        });
        if (this.category && this.category.slug) {
            this.categoryService.findBySlug(this.category.slug).subscribe(category => {
                this.category = category;
                this.loadForums();
            }, error => {
                AppUtil.handleErros(error, this.toast);
                this.loadingCategory = false;
            });
        }
    }

    loadForums(event?: any) {
        this.forumService.listForums(this.createFilter(event)).subscribe(data => {
            this.forums = [...data.list];
            this.total = data.total;
            this.loadingCategory = false;
        }, error => {
            AppUtil.handleErros(error);
            this.loadingCategory = false;
        });
    }

    paginator(event?: any) {
        this.loadForums(event);
    }

    createFilter(event?: any) {
        return {
            first: event ? event.first : 0,
            rows: event ? event.rows : this.rows,
            category: this.category
        };
    }

    trustHtml(value: string) {
        if (value) {
            return this.sanitizer.bypassSecurityTrustHtml(value);
        }
        return '';
    }

    trustImage(value: string) {
        return this.sanitizer.bypassSecurityTrustUrl(value);
    }

    canCreateForum(): boolean {
        if (this.user && this.user.group) {
            const index = environment.FORUM_GROUP_ID.findIndex(id => {
                return this.user.group.id === id;
            });
            return index >= 0;
        }
        return false;
    }

    forumCreated(forumSaved: Forum) {
        if (this.category && this.category.forums) {
            this.category.forums.push(forumSaved);
        }
    }

    canDeleteCategory() {
        try {
            return this.user.group.id === this.category.group.id;
        } catch (e) {
        }
        return false;
    }

    deleteCategory() {
        if (this.canDeleteCategory()) {
            this.confirmationService.confirm({
                message: this.lang.getDeleteCategoryInfo()
                    .replace('{name}', this.category.name)
                    .replace('{slug}', this.category.slug),
                accept: () => {
                    this.util.blockUI();
                    this.categoryService.delete(this.user, this.category).subscribe(() => {
                        this.router.navigate(['/']).then(() => {
                            this.util.unblockUI();
                        });
                    }, () => {
                        this.router.navigate(['/']).then(() => {
                            this.util.unblockUI();
                            this.toast.warn('Fail to delete category! Try again later.');
                        });
                    });
                },
                acceptIcon: 'pi pi-check',
                acceptLabel: this.lang.getConfirm(),
                rejectIcon: 'pi pi-times',
                rejectLabel: this.lang.getCancel()
            });
        } else {
            this.toast.warn(this.lang.getDeleteCategoryNoPermission());
        }
    }

}
