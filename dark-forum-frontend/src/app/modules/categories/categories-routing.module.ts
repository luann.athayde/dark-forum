import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from './category/category.component';
import { ListCategoriesComponent } from './list-categories/list-categories.component';

const routes: Routes = [
    {
        path: 'category/:slug',
        component: CategoryComponent
    },
    {
        path: 'all',
        component: ListCategoriesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CategoriesRoutingModule {
}
