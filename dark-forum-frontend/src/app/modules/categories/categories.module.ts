import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoryComponent } from './category/category.component';
import { ListCategoriesComponent } from './list-categories/list-categories.component';
import { CreateCategoryComponent } from './create-category/create-category.component';
import { ForumsModule } from '../forums/forums.module';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
    declarations: [CategoryComponent, ListCategoriesComponent, CreateCategoryComponent],
    exports: [
        ListCategoriesComponent,
        CreateCategoryComponent
    ],
    imports: [
        CommonModule,
        CategoriesRoutingModule,
        ForumsModule,
        CardModule,
        ButtonModule,
        SplitButtonModule,
        PaginatorModule,
        DialogModule,
        InputTextModule,
        CheckboxModule,
        ToggleButtonModule,
        ProgressSpinnerModule,
        TooltipModule
    ]
})
export class CategoriesModule {
}
