import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Language } from '../../../shared/locale/language';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { AppUtil } from '../../../shared/util/app.util';
import { User } from '../../../shared/model/user.model';
import { Category } from '../../../shared/model/category.model';
import { CategoryService } from '../../../shared/service/category.service';
import { ToasterService } from '../../../shared/service/toaster.service';

/**
 *
 */
@Component({
    selector: 'app-create-category',
    templateUrl: './create-category.component.html',
    styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent implements OnInit {

    @Output()
    categoryCreated: EventEmitter<Category> = new EventEmitter<Category>();

    @Input()
    category: Category = new Category();

    lang: Language = LanguageManager.getInstance().getLanguage();
    util: AppUtil = AppUtil.getInstance();
    user: User = User.getInstance();

    modalState = false;

    constructor(private toast: ToasterService,
                private categoryService: CategoryService) {
    }

    ngOnInit() {
    }

    showCreateCategoryModal() {
        this.modalState = true;
    }

    hideCreateCategoryModal() {
        this.category = new Category();
        this.modalState = false;
    }

    createSlug() {
        if (this.category.name) {
            this.category.slug = this.category.name
                .toLowerCase()
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, '')
                .replace(/\s/g, '-');
        }
    }

    saveCategory() {
        this.modalState = false;
        this.util.blockUI();
        this.category.user = {id: this.user.id};
        this.category.group = {id: 1};
        console.log('category -> ', this.category);
        this.categoryService.save(this.category).subscribe(saved => {
            this.toast.success(this.lang.getCategorySaved());
            this.util.unblockUI();
            this.categoryCreated.emit(saved);
            this.category = new Category();
        }, error => {
            AppUtil.handleErros(error, this.toast);
            this.util.unblockUI();
            this.category = new Category();
        });
    }

}
