import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Category } from '../../../shared/model/category.model';
import { Language } from '../../../shared/locale/language';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { User } from '../../../shared/model/user.model';
import { environment } from '../../../../environments/environment';
import { AppUtil } from '../../../shared/util/app.util';
import { CreateCategoryComponent } from '../create-category/create-category.component';
import { ToasterService } from '../../../shared/service/toaster.service';
import { CategoryService } from '../../../shared/service/category.service';

@Component({
    selector: 'app-list-categories',
    templateUrl: './list-categories.component.html',
    styleUrls: ['./list-categories.component.scss']
})
export class ListCategoriesComponent implements OnInit {

    @ViewChild('createCategory', {static: true})
    createCategory: CreateCategoryComponent;
    @Input()
    maxCategories = 5;

    lang: Language = LanguageManager.getInstance().getLanguage();
    util: AppUtil = AppUtil.getInstance();
    user: User = User.getInstance();

    loadingCategories = true;
    categories: Category[] = [];
    totalCategories = 0;

    constructor(private toast: ToasterService,
                private categoryService: CategoryService) {
    }

    ngOnInit(): void {
        this.listCategories({first: 0, rows: this.maxCategories});
    }

    listCategories(event: any = {first: 0, rows: 5}) {
        this.loadingCategories = true;
        this.categoryService.listCategories(event).subscribe(data => {
            this.categories = [...data.list];
            this.totalCategories = data.total;
            this.loadingCategories = false;
        }, error => {
            AppUtil.handleErros(error, this.toast);
        });
    }

    changePage(event: any) {
        if (event) {
            this.listCategories(event);
        }
    }

    canCreateCategory(): boolean {
        const index = environment.CATEGORY_GROUP_ID.findIndex(id => {
            return this.user.group.id === id;
        });
        return index >= 0;
    }

}
