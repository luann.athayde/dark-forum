import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventsRoutingModule } from './events-routing.module';
import { EventComponent } from './event/event.component';
import { SmallEventComponent } from './small-event/small-event.component';
import { ListEventsComponent } from './list-events/list-events.component';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { TooltipModule } from 'primeng/tooltip';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

@NgModule({
    declarations: [EventComponent, SmallEventComponent, ListEventsComponent],
    exports: [
        SmallEventComponent
    ],
    imports: [
        CommonModule,
        EventsRoutingModule,
        CalendarModule,
        CardModule,
        TooltipModule,
        ProgressSpinnerModule
    ]
})
export class EventsModule {
}
