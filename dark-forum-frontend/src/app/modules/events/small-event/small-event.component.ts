import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Event } from '../../../shared/model/event.model';
import { AppUtil } from '../../../shared/util/app.util';
import { User } from '../../../shared/model/user.model';
import { EventService } from '../../../shared/service/event.service';
import { LanguageManager } from '../../../shared/locale/language-manager';

@Component({
    selector: 'app-small-event',
    templateUrl: './small-event.component.html',
    styleUrls: ['./small-event.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SmallEventComponent implements OnInit {

    lang = LanguageManager.getInstance().getLanguage();

    util: AppUtil = AppUtil.getInstance();
    user: User = User.getInstance();
    events: Event[] = [];
    loadingEvents = true;
    baseDate = new Date();
    showEvents = true;

    constructor(private eventService: EventService) {
    }

    ngOnInit(): void {
        this.loadingEvents = true;
        for (let i = 1; i <= 16; i++) {
            this.events.push({
                id: i,
                eventDate: new Date(this.baseDate),
                banner: '',
                description: 'Evento ' + i,
                name: 'Evento ' + i,
                topic: null,
                user: this.user,
                createDate: new Date()
            });
            this.baseDate = new Date(this.baseDate.setUTCDate(this.baseDate.getUTCDate() + 1));
        }
        this.loadingEvents = false;
    }

    getDayOfWeek(day: number) {
        switch (day) {
            case 0:
                return 'Dom';
            case 1:
                return 'Seg';
            case 2:
                return 'Ter';
            case 3:
                return 'Qua';
            case 4:
                return 'Qui';
            case 5:
                return 'Sex';
            case 6:
                return 'Sáb';
        }
        return '-';
    }

}
