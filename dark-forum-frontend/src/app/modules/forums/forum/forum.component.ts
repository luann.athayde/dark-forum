import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Forum } from '../../../shared/model/forum.model';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { Language } from '../../../shared/locale/language';
import { ForumService } from '../../../shared/service/forum.service';
import { AppUtil } from '../../../shared/util/app.util';
import { environment } from '../../../../environments/environment';
import { User } from '../../../shared/model/user.model';
import { CreateForumComponent } from '../create-forum/create-forum.component';
import { MenuItem } from 'primeng/api';
import { Topic } from '../../../shared/model/topic.model';
import { TopicService } from '../../../shared/service/topic.service';

@Component({
    selector: 'app-forum',
    templateUrl: './forum.component.html',
    styleUrls: ['./forum.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ForumComponent implements OnInit {

    @ViewChild('createForum', {static: true})
    createForum: CreateForumComponent;

    lang: Language = LanguageManager.getInstance().getLanguage();
    util: AppUtil = AppUtil.getInstance();
    user: User = User.getInstance();
    filters: MenuItem[];
    loadingForum = false;

    @Input() forum: Forum = new Forum();
    @Input() limit = 3;

    topics: Topic[] = [];
    total = 0;
    rows = 10;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private sanitizer: DomSanitizer,
                private forumService: ForumService,
                private topicService: TopicService) {
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(params => {
            const slug = params.get('slug');
            if (slug) {
                // ---
                this.loadingForum = true;
                this.forumService.findBySlug(slug).subscribe(forum => {
                    Object.assign(this.forum, forum);
                    this.loadTopics();
                }, () => {
                    this.router.navigate(['/']).then(() => {
                        this.loadingForum = false;
                    });
                });
            } else {
                this.router.navigate(['/']).then();
            }
        });
        // ---
        this.filters = [...this.lang.listTopicFilters()];
        // ---
    }

    loadTopics(event?: any) {
        this.topicService.listTopics(this.createFilter(event)).subscribe(data => {
            this.topics = [...data.list];
            this.total = data.total;
            this.loadingForum = false;
        }, error => {
            AppUtil.handleErros(error);
            this.loadingForum = false;
        });
    }

    paginator(event?: any) {
        this.loadTopics(event);
    }

    createFilter(event?: any) {
        return {
            first: event ? event.first : 0,
            rows: event ? event.rows : this.rows,
            forum: this.forum
        };
    }

    trustHtml(value: string) {
        if (value) {
            try {
                return this.sanitizer.bypassSecurityTrustHtml(value);
            } catch (error) {
                AppUtil.handleErros(error);
            }
        }
        return '';
    }

    trustImage(value: string) {
        if (value) {
            try {
                return this.sanitizer.bypassSecurityTrustUrl(value);
            } catch (error) {
                AppUtil.handleErros(error);
            }
        }
        return '';
    }

    isForumEmpty(): boolean {
        try {
            return !this.topics || this.topics.length === 0;
        } catch (e) {}
        return false;
    }

    canCreateTopic(): boolean {
        if (this.user && this.user.group) {
            const index = environment.TOPIC_GROUP_ID.findIndex(id => {
                return this.user.group.id === id;
            });
            return index >= 0 && this.forum && !this.forum.locked;
        }
        return false;
    }

    canEditForum(): boolean {
        if (this.user && this.user.group) {
            const index = environment.FORUM_GROUP_ID.findIndex(id => {
                return this.user.group.id === id;
            });
            return index >= 0;
        }
        return false;
    }

    editForum() {
        const forum = new Forum();
        Object.assign(forum, this.forum);
        this.createForum.showCreateForumModal(this.forum.category, forum);
    }

    reloadForum(forum: Forum) {
        // ---
        Object.assign(this.forum, forum);
    }

}
