import { Component, Input, OnInit } from '@angular/core';
import { Category } from '../../../shared/model/category.model';
import { Forum } from '../../../shared/model/forum.model';

@Component({
    selector: 'app-list-forums',
    templateUrl: './list-forums.component.html',
    styleUrls: ['./list-forums.component.scss']
})
export class ListForumsComponent implements OnInit {

    @Input() category: Category;
    forums: Forum[] = [];

    constructor() {
    }

    ngOnInit(): void {

        this.forums.push({
            id: 1,
            name: 'teste 1',
            slug: 'teste-1',
            category: this.category,
            topics: [],
            createDate: new Date(),
            totalTopics: 0,
            user: {
                name: 'Luann Athayde'
            }
        });

    }

    changePage(event: any) {

    }

}
