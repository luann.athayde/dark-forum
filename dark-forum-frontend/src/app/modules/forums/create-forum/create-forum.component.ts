import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Language } from '../../../shared/locale/language';
import { LanguageManager } from '../../../shared/locale/language-manager';
import { AppUtil } from '../../../shared/util/app.util';
import { User } from '../../../shared/model/user.model';
import { ToasterService } from '../../../shared/service/toaster.service';
import { Forum } from '../../../shared/model/forum.model';
import { Category } from '../../../shared/model/category.model';
import { ForumService } from '../../../shared/service/forum.service';

@Component({
    selector: 'app-create-forum',
    templateUrl: './create-forum.component.html',
    styleUrls: ['./create-forum.component.scss']
})
export class CreateForumComponent implements OnInit {

    @Output()
    forumCreated: EventEmitter<Forum> = new EventEmitter<Forum>();

    lang: Language = LanguageManager.getInstance().getLanguage();
    util: AppUtil = AppUtil.getInstance();
    user: User = User.getInstance();

    modalState = false;
    category: Category;
    forum: Forum = new Forum();

    constructor(private toast: ToasterService,
                private forumService: ForumService) {
    }

    ngOnInit() {
    }

    showCreateForumModal( category: Category, forum = new Forum() ) {
        this.category = category;
        Object.assign(this.forum, forum);
        this.modalState = true;
    }

    hideCreateForumModal() {
        this.category = null;
        this.modalState = false;
    }

    createSlug() {
        if (this.forum.name) {
            this.forum.slug = this.forum.name
                .toLowerCase()
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, '')
                .replace(/\s/g, '-');
        }
    }

    saveForum() {
        this.modalState = false;
        this.util.blockUI();

        this.forum.category = {id: this.category.id};
        this.forum.user = {id: this.user.id};
        this.forum.group = {id: 1};

        this.forumService.save(this.forum).subscribe(savedForum => {
            this.forum = savedForum;
            this.forumCreated.emit(savedForum);
            this.util.unblockUI();
        }, error => {
            AppUtil.handleErros(error, this.toast);
            this.util.unblockUI();
        });

    }

}
