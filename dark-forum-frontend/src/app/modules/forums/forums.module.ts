import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForumsRoutingModule } from './forums-routing.module';
import { ForumComponent } from './forum/forum.component';
import { ListForumsComponent } from './list-forums/list-forums.component';
import { CreateForumComponent } from './create-forum/create-forum.component';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TooltipModule } from 'primeng/tooltip';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

@NgModule({
    declarations: [ForumComponent, ListForumsComponent, CreateForumComponent],
    exports: [
        CreateForumComponent,
        ListForumsComponent
    ],
    imports: [
        CommonModule,
        ForumsRoutingModule,
        CardModule,
        ButtonModule,
        SplitButtonModule,
        PaginatorModule,
        DialogModule,
        ToggleButtonModule,
        InputTextModule,
        InputTextareaModule,
        TooltipModule,
        ProgressSpinnerModule
    ]
})
export class ForumsModule {
}
