import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListTagsComponent } from './list-tags/list-tags.component';

const routes: Routes = [
    {
        path: 'list-tags',
        component: ListTagsComponent
    },
    {
        path: 'list-tags/:id',
        component: ListTagsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TagsRoutingModule {
}
