import { Component, Input, OnInit } from '@angular/core';
import { Tag } from '../../../shared/model/tag.model';

@Component({
    selector: 'app-tag',
    templateUrl: './tag.component.html',
    styleUrls: ['./tag.component.scss']
})
export class TagComponent implements OnInit {

    @Input()
    tag: Tag = new Tag();

    constructor() {
    }

    ngOnInit(): void {
    }

}
