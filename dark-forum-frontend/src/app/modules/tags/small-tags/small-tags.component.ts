import { Component, Input, OnInit } from '@angular/core';
import { Tag } from '../../../shared/model/tag.model';

@Component({
    selector: 'app-small-tags',
    templateUrl: './small-tags.component.html',
    styleUrls: ['./small-tags.component.scss']
})
export class SmallTagsComponent implements OnInit {

    @Input()
    tags: Tag[] = [];

    constructor() {
    }

    ngOnInit(): void {
    }

}
