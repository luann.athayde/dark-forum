import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTagsComponent } from './list-tags/list-tags.component';
import { TagComponent } from './tag/tag.component';
import { SmallTagsComponent } from './small-tags/small-tags.component';
import { RouterModule } from '@angular/router';
import { TagsRoutingModule } from './tags-routing.module';

@NgModule({
    declarations: [ListTagsComponent, TagComponent, SmallTagsComponent],
    exports: [
        SmallTagsComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        TagsRoutingModule
    ]
})
export class TagsModule {
}
