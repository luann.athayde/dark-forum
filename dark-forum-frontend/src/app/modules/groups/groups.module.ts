import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupsRoutingModule } from './groups-routing.module';
import { GroupComponent } from './group/group.component';

@NgModule({
    declarations: [GroupComponent],
    imports: [
        CommonModule,
        GroupsRoutingModule
    ]
})
export class GroupsModule {
}
