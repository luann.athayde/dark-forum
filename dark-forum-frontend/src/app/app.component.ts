import { AfterViewInit, Component, EventEmitter, OnInit } from '@angular/core';
import { LanguageManager } from './shared/locale/language-manager';
import { Language } from './shared/locale/language';
import { FOOTER } from './shared/model/footer.model';
import { User } from './shared/model/user.model';
import { AppUtil } from './shared/util/app.util';
import { environment } from '../environments/environment';
import { UserService } from './shared/service/user.service';
import { Md5 } from 'ts-md5';
import { ToasterService } from './shared/service/toaster.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { MenuItem } from 'primeng/api';
import { OverlayPanel } from 'primeng/overlaypanel';
import { PtBrLang } from './shared/locale/pt-br.lang';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

    static breadcrumbEvent: EventEmitter<MenuItem[]> = new EventEmitter<MenuItem[]>();

    user: User = User.getInstance();
    util: AppUtil = AppUtil.getInstance();
    lang: Language;
    forumName = 'DarkFireRO - Fórum';
    forumQuote = 'Venha aventurar-se no mundo de Ragnarok.';
    footer: string[] = FOOTER;

    bannerLink: string;
    menuItems: MenuItem[] = [];
    breadcrumb: MenuItem[] = [];
    languages: MenuItem[] = [];

    showSubTopics = false;

    constructor(private route: Router,
                private titleService: Title,
                private toast: ToasterService,
                private userService: UserService) {
    }

    ngOnInit() {
        console.log('Initializing app...');
        // sessionStorage.setItem('account', JSON.stringify(DEFAULT_ACCOUNT));
        this.titleService.setTitle(this.forumName);
        this.initLanguage();
        this.bannerLink = 'https://darkfirero.net';
        // ---
        if (AppComponent.breadcrumbEvent) {
            AppComponent.breadcrumbEvent.subscribe((breadcrumb: MenuItem[]) => {
                this.updateBreadcrumb(breadcrumb);
            });
        }
    }

    ngAfterViewInit() {
        this.initUser();
    }

    initLanguage() {
        LanguageManager.getInstance().setLanguage(PtBrLang.getInstance());
        this.lang = LanguageManager.getInstance().getLanguage();
        const savedLangStr = sessionStorage.getItem('language');
        if (savedLangStr && savedLangStr.length > 0) {
            const selectedLang: Language = LanguageManager.getInstance().listLanguages().find(
                lang => lang.getId() === savedLangStr
            );
            if (selectedLang) {
                LanguageManager.getInstance().setLanguage(selectedLang);
                this.lang = LanguageManager.getInstance().getLanguage();
            }
        }
        // ---
        LanguageManager.getInstance().listLanguages().forEach(lang => {
            this.languages.push({
                id: lang.getId(),
                label: lang.getName(),
                icon: lang.getIcon(),
                command: (event) => {
                    this.changeLanguage(event);
                }
            });
        });
    }

    changeLanguage(event) {
        console.log('Changing default app language...');
        if (event && event.item && event.item.id) {
            const selectedLang: Language = LanguageManager.getInstance().listLanguages().find(
                lang => lang.getId() === event.item.id
            );
            console.log('selectedLang -> ', selectedLang);
            if (selectedLang) {
                LanguageManager.getInstance().setLanguage(selectedLang);
                this.lang = LanguageManager.getInstance().getLanguage();
                sessionStorage.setItem('language', this.lang.getId());
            }
            window.location.reload();
        }
    }

    initUser() {
        try {
            if (environment.USE_RAGNAROK_LOGIN) {
                this.ragnarokUser();
            } else {
                this.forumUser();
            }
        } catch (e) {
            console.warn('Fail to init user...', e);
        }
    }

    forumUser() {
        const accountJson = sessionStorage.getItem('account');
        if (accountJson && accountJson.length > 0) {
            const account: User = JSON.parse(accountJson);
            this.user.set(account);
            this.util.blockUI();
            // ---
            this.userService.findById(account.id).subscribe(user => {
                this.updateLoggedUser(user);
            }, error => {
                AppUtil.handleErros(error, this.toast);
                this.util.unblockUI();
            });
        } else {
            this.initMenu();
        }
    }

    ragnarokUser() {
        const accountJson = sessionStorage.getItem('account');
        if (accountJson && accountJson.length > 0) {
            const account = JSON.parse(accountJson);
            this.user.id = account.id;
            this.util.blockUI();
            // ---
            this.userService.userByRagnarokAccount(account).subscribe(user => {
                this.updateLoggedUser(user);
            }, error => {
                AppUtil.handleErros(error, this.toast);
                this.user.set({});
                this.initMenu();
                this.util.unblockUI();
            });
        } else {
            this.initMenu();
        }
    }

    updateLoggedUser(user: User) {
        this.user.set(user);
        this.initMenu();
        this.util.unblockUI();
    }

    onLoginPanel(loginOverlayPanel: OverlayPanel, loginButton: HTMLDivElement) {
        if (this.user.id && this.user.id > 0) {
            this.logout();
        } else {
            loginOverlayPanel.show({}, loginButton);
        }
    }

    login(overlayPanel: OverlayPanel) {
        overlayPanel.hide();
        this.util.blockUI();
        // ---
        const userid = this.user.userid;
        const password = Md5.hashStr(this.user.password).toString();
        this.user.set({});
        this.userService.findByUserAndPassword(userid, password).subscribe(
            user => {
                this.updateLoggedUser(user);
            }, error => {
                AppUtil.handleErros(error, this.toast);
                this.util.unblockUI();
            });
    }

    logout() {
        this.util.blockUI();
        this.userService.updateUser({id: this.user.id}).subscribe(() => {
            this.route.navigate(['/']).then(() => {
                this.updateLoggedUser(new User());
            });
        }, (error) => {
            AppUtil.handleErros(error, this.toast);
            this.route.navigate(['/']).then(() => {
                this.updateLoggedUser(new User());
            });
        });
    }

    initMenu() {
        const menu: MenuItem[] = [];
        this.lang.listMenu().forEach(menuItem => {
            if (!menuItem.state) {
                menu.push(menuItem);
            } else if (menuItem.state && menuItem.state.requiredLogin && this.user.id > 0) {
                if (menuItem.state.group) {
                    if (this.user.group && menuItem.state.group === this.user.group.id) {
                        menu.push(menuItem);
                    }
                } else {
                    menu.push(menuItem);
                }
            }
        });
        this.menuItems = [...menu];
    }

    updateBreadcrumb(breadcrumb: MenuItem[]) {
        const items: MenuItem[] = [{
            id: '0',
            icon: 'pi pi-home',
            url: '/'
        }];
        breadcrumb.forEach(item => {
            items.push({
                label: item.label,
                url: item.url
            });
        });
        this.breadcrumb = [...items];
    }

}
