import { Group } from './group.model';

export class User {

    private static INSTANCE: User = new User();

    id?: number;
    group?: Group = new Group();
    userid?: string;
    password?: string;
    name?: string;
    birthday?: Date;
    bio?: string;
    profilePicture?: string;
    loginId?: number;

    level?: string;
    online ? = false;
    totalPosts ? = 0;

    public static getInstance() {
        return User.INSTANCE;
    }

    public set?(user: User, saveOnSession = true) {
        this.id = user.id;
        this.group = user.group;
        this.userid = user.userid;
        this.password = null;
        this.name = user.name;
        this.birthday = user.birthday;
        this.bio = user.bio;
        this.profilePicture = user.profilePicture;
        this.loginId = user.loginId;
        this.level = user.level;
        this.online = user.online;
        this.totalPosts = user.totalPosts;
        if (user && user.id && user.id > 0 && saveOnSession) {
            sessionStorage.setItem('account', JSON.stringify(user));
        } else {
            sessionStorage.removeItem('account');
        }
    }

}
