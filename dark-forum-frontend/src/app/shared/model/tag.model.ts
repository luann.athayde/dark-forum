import { User } from './user.model';

export class Tag {

    id?: number;
    name?: string;
    user?: User;

}

