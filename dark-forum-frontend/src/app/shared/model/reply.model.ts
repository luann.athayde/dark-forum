import { Topic } from './topic.model';
import { User } from './user.model';

export class Reply {

    id?: number;
    topic?: Topic;
    body?: string;
    user?: User;
    createDate?: Date;

}

