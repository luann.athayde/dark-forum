import { User } from './user.model';
import { Group } from './group.model';
import { Forum } from './forum.model';

export class Category {

    id?: number;
    name?: string;
    slug?: string;
    pinned?: boolean;
    user?: User;
    group?: Group;
    createDate?: Date;

    forums?: Forum[] = [];

}

