import { User } from './user.model';
import { Tag } from './tag.model';
import { Topic } from './topic.model';

export class TopicTag {

    id?: number;

    tag?: Tag;
    topic?: Topic;

    user?: User;

}

