import { User } from './user.model';
import { Group } from './group.model';
import { Category } from './category.model';
import { Topic } from './topic.model';

export class Forum {

    id?: number;
    category?: Category;
    name?: string;
    description?: string;
    slug?: string;
    pinned?: boolean;
    locked?: boolean;
    totalTopics?: number;
    totalViews?: number;
    user?: User;
    group?: Group;
    createDate?: Date;

    topics?: Topic[] = [];

}

