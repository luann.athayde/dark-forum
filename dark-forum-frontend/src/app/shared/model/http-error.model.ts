/**
 *
 */
import { Message } from './message.model';

export class HttpError {
    status: number;
    statusText: string;
    name: string;
    ok: boolean;
    message: string;
    error: Message;
}
