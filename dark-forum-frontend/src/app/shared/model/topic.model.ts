import { User } from './user.model';
import { Forum } from './forum.model';
import { Reply } from './reply.model';
import { TopicTag } from './topic-tag.model';

export class Topic {

    id?: number;
    forum?: Forum = new Forum();
    name?: string;
    slug?: string;
    body?: string;

    views?: number;

    locked?: boolean;
    user?: User = new User();
    createDate?: Date;

    rating?: number;

    tags?: TopicTag[] = [];

    replies?: Reply[] = [];

}

