import { User } from './user.model';
import { Topic } from './topic.model';

export class Event {

    id?: number;
    topic?: Topic;
    name?: string;
    description?: string;
    banner?: string;
    user?: User;
    createDate?: Date;
    eventDate?: Date;

}

