/**
 *
 */
export class Message {
    message: string;
    status: string;
    stackTrace: string;
}
