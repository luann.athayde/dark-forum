import { HttpParams } from '@angular/common/http';

export class QueryParams {

    private parametros: HttpParams;

    constructor() {
        this.parametros = new HttpParams();
    }

    add(param: string, value: any): QueryParams {
        if (value != null) {
            if (value instanceof Date) {
                const data = new Date(value);
                this.parametros = this.parametros.set(param, data.toISOString());
            } else {
                this.parametros = this.parametros.set(param, value.toString());
            }
        }
        return this;
    }

    build(): HttpParams {
        return this.parametros;
    }

}
