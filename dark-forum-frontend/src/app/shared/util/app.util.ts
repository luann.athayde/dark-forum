/**
 * Basic Util
 */
import { HttpError } from '../model/http-error.model';
import { ToasterService } from '../service/toaster.service';
import { SERVICO_INDISPONIVEL } from '../mensagens';
import { environment } from '../../../environments/environment';
import { Topic } from '../model/topic.model';

export class AppUtil {

    private static INSTANCE = new AppUtil();

    blockedUI = false;

    public static onlyNumbers(value: any) {
        return value;
    }

    static handleErros(httpError: HttpError, toast?: ToasterService) {
        console.error('Operation failed -> ', httpError);
        let msg = SERVICO_INDISPONIVEL;
        if (httpError.error && httpError.error.message) {
            msg = httpError.error.message;
        }
        if (toast) {
            toast.warn(msg);
        }
    }

    static getInstance() {
        return AppUtil.INSTANCE;
    }

    blockUI() {
        this.blockedUI = true;
    }

    unblockUI() {
        setTimeout(() => {
            this.blockedUI = false;
        }, 500);
    }

    getTopicTags(topic: Topic) {
        try {
            return topic.tags.map(topicTag => topicTag.tag);
        } catch (e) {
            AppUtil.handleErros(e);
        }
        return [];
    }

    getRandomData(min = 1) {
        return [
            Math.floor(Math.random() * 4) + min,
            Math.floor(Math.random() * 11) + min,
            Math.floor(Math.random() * 7) + min,
            Math.floor(Math.random() * 5) + min,
            Math.floor(Math.random() * 12) + min
        ];
    }

}
