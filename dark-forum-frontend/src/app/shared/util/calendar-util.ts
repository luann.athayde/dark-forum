export class CalendarUtil {

    static ptBrLanguage: any;

    static getCalendarLanguage() {
        if ( !CalendarUtil.ptBrLanguage ) {
            CalendarUtil.ptBrLanguage = {
                firstDayOfWeek: 0,
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                dayNamesMin: ['Do', 'Se', 'Te', 'Qa', 'Qi', 'Se', 'Sa'],
                monthNames: [
                    'Janeiro', 'Fevereiro', 'Março',
                    'Abril', 'Maio', 'Junho',
                    'Julho', 'Agosto', 'Setembro',
                    'Outubro', 'Novembro', 'Dezembro'
                ],
                monthNamesShort: [
                    'Jan', 'Fev', 'Mar',
                    'Abr', 'Mai', 'Jun',
                    'Jul', 'Ago', 'Set',
                    'Out', 'Nov', 'Dez'],
                today: 'Home',
                clear: 'Limpar',
                dateFormat: 'dd/mm/yyyy',
                weekHeader: 'Semana'
            };
        }
        return CalendarUtil.ptBrLanguage;
    }

    static convertDate(data: string): Date {
        try {
            // ----
            const dhValue = new Date(data);
            // ---
            return new Date(
                dhValue.getTime() + (dhValue.getTimezoneOffset() * 60000)
            );
        } catch (e) {
            console.error('CalendarUtils::date converter -> ', e);
        }
        return null;
    }

}
