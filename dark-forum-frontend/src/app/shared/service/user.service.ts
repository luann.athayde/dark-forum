import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { User } from '../model/user.model';
import { QueryParams } from '../util/query.params';

/**
 *
 */
@Injectable({
    providedIn: 'root'
})
export class UserService {

    private apiUser = `${environment.URL_API}/user`;

    constructor(private httpClient: HttpClient) {
    }

    findByUserAndPassword(userid: string, password: string): Observable<User> {
        const params = new QueryParams();
        params.add('userid', userid);
        params.add('password', password);
        return this.httpClient.get<User>(
            `${this.apiUser}/login`,
            {params: params.build()}
        );
    }

    userByRagnarokAccount(login): Observable<User> {
        return this.httpClient.post<User>(
            `${this.apiUser}/ragnarok/account`,
            login
        );
    }

    findById(id: number): Observable<User> {
        return this.httpClient.post<User>(
            `${this.apiUser}`,
            {id}
        );
    }

    listOnlineUsers(): Observable<User[]> {
        return this.httpClient.get<User[]>(
            `${this.apiUser}/online`
        );
    }

    updateUser(user: User): Observable<User> {
        return this.httpClient.post<User>(
            `${this.apiUser}`,
            user
        );
    }

}

