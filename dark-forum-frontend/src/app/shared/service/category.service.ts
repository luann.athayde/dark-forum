import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Category } from '../model/category.model';
import { QueryParams } from '../util/query.params';
import { User } from '../model/user.model';

/**
 *
 */
@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    private apiCategory = `${environment.URL_API}/category`;

    constructor(private httpClient: HttpClient) {
    }

    listCategories(event: any): Observable<{ list: Category[], total: number }> {
        const params = new QueryParams();
        params.add('first', event.first);
        params.add('rows', event.rows);
        return this.httpClient.get<{ list: Category[], total: number }>(
            `${this.apiCategory}`,
            { params: params.build() }
        );
    }

    findBySlug( slug: string): Observable<Category> {
        return this.httpClient.get(
            `${this.apiCategory}/${slug}`
        );
    }

    save( category: Category ): Observable<Category> {
        return this.httpClient.post<Category>(
            `${this.apiCategory}`,
            category
        );
    }

    delete( user: User, category: Category ): Observable<Category> {
        const params = new QueryParams();
        params.add('userId', user.id);
        return this.httpClient.delete<Category>(
            `${this.apiCategory}/${category.slug}`,
            { params: params.build() }
        );
    }

}

