import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { QueryParams } from '../util/query.params';
import { Forum } from '../model/forum.model';

/**
 *
 */
@Injectable({
    providedIn: 'root'
})
export class ForumService {

    private apiCategory = `${environment.URL_API}/forum`;

    constructor(private httpClient: HttpClient) {
    }

    listForums(event: any): Observable<{ list: Forum[], total: number }> {
        const params = new QueryParams();
        params.add('first', event.first);
        params.add('rows', event.rows);
        if (event.category) {
            params.add('category', event.category.id);
        }
        return this.httpClient.get<{ list: Forum[], total: number }>(
            `${this.apiCategory}`,
            {params: params.build()}
        );
    }

    save(forum: Forum): Observable<Forum> {
        return this.httpClient.post<Forum>(
            `${this.apiCategory}`,
            forum
        );
    }

    findBySlug(slug: string): Observable<Forum> {
        return this.httpClient.get<Forum>(
            `${this.apiCategory}/${slug}`
        );
    }

}

