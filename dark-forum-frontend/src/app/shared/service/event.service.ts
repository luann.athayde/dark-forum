import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Event } from '../model/event.model';

/**
 *
 */
@Injectable({
    providedIn: 'root'
})
export class EventService {

    private apiEvent = `${environment.URL_API}/event`;

    constructor(private httpClient: HttpClient) {
    }

    save(event: Event): Observable<Event> {
        return this.httpClient.post<Event>(
            `${this.apiEvent}`,
            event
        );
    }

}

