import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
    providedIn: 'root'
})
export class ToasterService {

    constructor(private mensagemService: MessageService) {
    }

    public success(mensagem: string) {
        this.mensagemService.add({severity: 'success', detail: mensagem});
    }

    public info(mensagem: string) {
        this.mensagemService.add({severity: 'info', detail: mensagem});
    }

    public warn(mensagem: string) {
        this.mensagemService.add({severity: 'warn', detail: mensagem});
    }

    public erro(mensagem: string) {
        this.mensagemService.add({severity: 'error', detail: mensagem});
    }

}
