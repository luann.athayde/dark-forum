import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { QueryParams } from '../util/query.params';
import { Topic } from '../model/topic.model';

/**
 *
 */
@Injectable({
    providedIn: 'root'
})
export class TopicService {

    private apiTopic = `${environment.URL_API}/topic`;

    constructor(private httpClient: HttpClient) {
    }

    listTopics(event: any): Observable<{ list: Topic[], total: number }> {
        const params = new QueryParams();
        params.add('first', event.first);
        params.add('rows', event.rows);
        params.add('hot', event.hot);
        if (event.forum) {
            params.add('idForum', event.forum.id);
        }
        if (event.topic) {
            params.add('idTopic', event.topic.id);
        }
        return this.httpClient.get<{ list: Topic[], total: number }>(
            `${this.apiTopic}`,
            {params: params.build()}
        );
    }

    save(topic: Topic): Observable<Topic> {
        return this.httpClient.post<Topic>(
            `${this.apiTopic}`,
            topic
        );
    }

    findBySlug(slug: string): Observable<Topic> {
        return this.httpClient.get<Topic>(
            `${this.apiTopic}/${slug}`
        );
    }

}

