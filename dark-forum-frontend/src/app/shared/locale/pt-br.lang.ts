import { Language } from './language';
import { MENU } from './menu.pt-br';
import { MenuItem } from 'primeng/api';

/**
 * Language: Brazilian Portuguese
 */
export class PtBrLang implements Language {

    public static getInstance(): Language {
        return new PtBrLang();
    }

    getId(): string {
        return '2';
    }

    getName(): string {
        return 'Português-BR';
    }

    getIcon(): string {
        return '';
    }

    getDateFormat(): string {
        return 'dd/MM/yyyy HH:mm:ss';
    }

    getHomePage(): string {
        return 'Inicio';
    }

    getCategories(): string {
        return 'Categorias';
    }

    getForums(): string {
        return 'Fóruns';
    }

    getTopics(): string {
        return 'Tópicos';
    }

    getHotTopics(): string {
        return 'Tópicos Ativos';
    }

    listMenu(): MenuItem[] {
        return MENU;
    }

    getSearch(): string {
        return 'Pesquisar';
    }

    getTotal(): string {
        return 'Total';
    }

    getOf(): string {
        return 'de';
    }

    getConfirmDialogTitle(): string {
        return 'Atenção';
    }

    getCategory(): string {
        return 'Categoria';
    }

    getForum(): string {
        return 'Fórum';
    }

    getTopic(): string {
        return 'Tópico';
    }

    getEmptyForums(): string {
        return 'Nenhum fórum encontrado para está categoria.';
    }

    getEmptyTopics(): string {
        return 'Nenhum tópico disponível para este fórum.';
    }

    getReplies(): string {
        return 'respostas';
    }

    getViews(): string {
        return 'visualizações';
    }

    getFilter(): string {
        return 'Filtrar';
    }

    getCreateCategory(): string {
        return 'Criar Categoria';
    }

    getCreateForum(): string {
        return 'Criar Fórum';
    }

    getCreateTopic(): string {
        return 'Criar Tópico';
    }

    getEditCategory(): string {
        return 'Editar Categoria';
    }

    getEditForum(): string {
        return 'Editar Fórum';
    }

    getEditTopic(): string {
        return 'Editar Tópico';
    }

    listTopicFilters(): MenuItem[] {
        return [
            {
                id: 'updated',
                label: 'Mais recente'
            },
            {
                id: 'title',
                label: 'Titulo do tópico'
            },
            {
                id: 'date',
                label: 'Data de criação'
            },
            {
                id: 'views',
                label: 'Mais visualizado'
            },
            {
                id: 'replies',
                label: 'Mais respondido'
            }
        ];
    }

    getBy(): string {
        return 'Por';
    }

    getPostAt(): string {
        return 'Postado em';
    }

    getPosts(): string {
        return 'posts';
    }

    getRepliedAt(): string {
        return 'Respondido em';
    }

    getLogin(): string {
        return '- Entrar -';
    }

    getUsername(): string {
        return 'Usuário';
    }

    getPassword(): string {
        return 'Senha';
    }

    getLoginMessage(): string {
        return '<h3>Efetue login para fazer um comentário</h3>' +
            'Você poderar escrever um comentário após realizar login';
    }

    getExistingUser(): string {
        return 'Tem um usuário? Logar-se';
    }

    getLogoutMessage(): string {
        return 'Deslogar';
    }

    getWhoIsOnline(): string {
        return 'Quem está Online';
    }

    getForumStatistics(): string {
        return 'Estatísticas do Forum';
    }

    getEvents(): string {
        return 'Eventos';
    }

    getSlug(): string {
        return 'Slug';
    }

    getLocked(): string {
        return 'Bloqueado';
    }

    getUnlocked(): string {
        return 'Desbloqueado';
    }

    getPinned(): string {
        return 'Fixado';
    }

    getConfirm(): string {
        return 'Confirmar';
    }

    getCancel(): string {
        return 'Cancelar';
    }

    getSave(): string {
        return 'Salvar';
    }

    getCategorySaved(): string {
        return 'Categoria criada com sucesso!';
    }

    getLoading(): string {
        return 'Carregando';
    }

    getDescription(): string {
        return 'Descrição';
    }

    getDeleteCategory(): string {
        return 'Excluir Categoria';
    }

    getDeleteCategoryInfo(): string {
        return 'Deseja realmente excluir a categoria {name} ({slug})?';
    }

    getDeleteCategoryNoPermission(): string {
        return 'Você não possui permissão para executar está ação!';
    }

    getTags(): string {
        return 'Tags';
    }

    getTagsList(): string {
        return 'Lista de tags do tópico...';
    }

    getAbout(): string {
        return 'Sobre';
    }

}
