import { MenuItem } from 'primeng/api';

export interface Language {

    getId(): string;
    getName(): string;
    getIcon(): string;
    getDateFormat(): string;

    getHomePage(): string;
    getCategories(): string;
    getForums(): string;
    getTopics(): string;
    getHotTopics(): string;
    listMenu(): MenuItem[];
    getSearch(): string;
    getTotal(): string;
    getOf(): string;

    getConfirmDialogTitle(): string;

    getCategory(): string;
    getForum(): string;
    getTopic(): string;

    getEmptyForums(): string;
    getEmptyTopics(): string;

    getReplies(): string;
    getViews(): string;
    getFilter(): string;
    getCreateCategory(): string;
    getCreateForum(): string;
    getCreateTopic(): string;

    getEditCategory(): string;
    getEditForum(): string;
    getEditTopic(): string;

    getBy(): string;
    getPostAt(): string;
    getPosts(): string;
    getRepliedAt(): string;
    listTopicFilters(): MenuItem[];

    getLoginMessage(): string;
    getLogin(): string;
    getUsername(): string;
    getPassword(): string;

    getExistingUser(): string;
    getLogoutMessage(): string;

    getWhoIsOnline(): string;
    getForumStatistics(): string;
    getEvents(): string;

    getSlug(): string;
    getPinned(): string;
    getLocked(): string;
    getUnlocked(): string;
    getConfirm(): string;
    getCancel(): string;
    getSave(): string;

    getDescription(): string;

    getCategorySaved(): string;

    getLoading(): string;

    getDeleteCategory(): string;
    getDeleteCategoryInfo(): string;
    getDeleteCategoryNoPermission(): string;

    getTags(): string;
    getTagsList(): string;

    getAbout(): string;

}

