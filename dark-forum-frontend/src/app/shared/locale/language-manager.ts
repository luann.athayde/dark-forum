import { Language } from './language';
import { PtBrLang } from './pt-br.lang';
import { EnUsLang } from './en-us.lang';

export class LanguageManager {

    static INSTANCE = new LanguageManager();

    private lang: Language = PtBrLang.getInstance();

    public static getInstance() {
        return LanguageManager.INSTANCE;
    }

    public getLanguage() {
        return this.lang;
    }
    public setLanguage(lang: Language) {
        this.lang = lang;
    }

    public listLanguages(): Language[] {
        return [
            EnUsLang.getInstance(),
            PtBrLang.getInstance()
        ];
    }

}

