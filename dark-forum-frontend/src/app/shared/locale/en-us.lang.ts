import { Language } from './language';
import { MENU } from './menu.en-us';
import { MenuItem } from 'primeng/api';

/**
 * Language: USA English
 */
export class EnUsLang implements Language {

    public static getInstance(): Language {
        return new EnUsLang();
    }

    getId(): string {
        return '1';
    }

    getName(): string {
        return 'English-US';
    }

    getIcon(): string {
        return '';
    }

    getDateFormat(): string {
        return 'MM/dd/yyyy HH:mm:ss';
    }

    getHomePage(): string {
        return 'Home Page';
    }

    getCategories(): string {
        return 'Categories';
    }

    getForums(): string {
        return 'Forums';
    }

    getTopics(): string {
        return 'Topics';
    }

    getHotTopics(): string {
        return 'Hot Topics';
    }

    listMenu(): MenuItem[] {
        return MENU;
    }

    getSearch(): string {
        return 'Search';
    }

    getTotal(): string {
        return 'Total';
    }

    getOf(): string {
        return 'of';
    }

    getConfirmDialogTitle(): string {
        return 'Confirm';
    }

    getCategory(): string {
        return 'Category';
    }

    getForum(): string {
        return 'Forum';
    }

    getTopic(): string {
        return 'Topic';
    }

    getEmptyForums(): string {
        return 'No forums found in this category.';
    }

    getEmptyTopics(): string {
        return 'No topics found in this forum.';
    }

    getReplies(): string {
        return 'replies';
    }

    getViews(): string {
        return 'views';
    }

    getFilter(): string {
        return 'Filter';
    }

    getCreateCategory(): string {
        return 'Create Category';
    }

    getCreateForum(): string {
        return 'Create Forum';
    }

    getCreateTopic(): string {
        return 'Create Topic';
    }

    getEditCategory(): string {
        return 'Edit Category';
    }
    getEditForum(): string {
        return 'Edit Forum';
    }
    getEditTopic(): string {
        return 'Edit Topic';
    }

    listTopicFilters(): MenuItem[] {
        return [
            {
                id: 'updated',
                label: 'Recently updated'
            },
            {
                id: 'title',
                label: 'Topic title'
            },
            {
                id: 'date',
                label: 'Create date'
            },
            {
                id: 'views',
                label: 'Most views'
            },
            {
                id: 'replies',
                label: 'More replies'
            }
        ];
    }

    getBy(): string {
        return 'By';
    }

    getPostAt(): string {
        return 'Posted at';
    }

    getPosts(): string {
        return 'posts';
    }

    getRepliedAt(): string {
        return 'Replied at';
    }

    getLogin(): string {
        return '- Log In -';
    }

    getUsername(): string {
        return 'Username';
    }

    getPassword(): string {
        return 'Password';
    }

    getLoginMessage(): string {
        return '<h3>Please sign in to comment</h3>' +
            'You will be able to leave a comment after signing in';
    }

    getExistingUser(): string {
        return 'Existing user? Sing';
    }

    getLogoutMessage(): string {
        return 'Logout';
    }

    getWhoIsOnline(): string {
        return 'Who\'s Online';
    }

    getForumStatistics(): string {
        return 'Forum Statistics';
    }

    getEvents(): string {
        return 'Events';
    }

    getSlug(): string {
        return 'Slug';
    }

    getLocked(): string {
        return 'Locked';
    }
    getUnlocked(): string {
        return 'Unlocked';
    }

    getPinned(): string {
        return 'Pinned';
    }

    getConfirm(): string {
        return 'Confirm';
    }

    getCancel(): string {
        return 'Cancel';
    }

    getSave(): string {
        return 'Save';
    }

    getCategorySaved(): string {
        return 'Category created succesfully!';
    }

    getLoading(): string {
        return 'Loading';
    }

    getDescription(): string {
        return 'Description';
    }

    getDeleteCategory(): string {
        return 'Delete Category';
    }

    getDeleteCategoryInfo(): string {
        return 'Confirm deletion of the category {name} ({slug})?';
    }

    getDeleteCategoryNoPermission(): string {
        return 'You dont have permission to perform this action!';
    }

    getTags(): string {
        return 'Tags';
    }

    getTagsList(): string {
        return 'Tags list for topic...';
    }

    getAbout(): string {
        return 'About';
    }

}
