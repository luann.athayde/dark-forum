/**
 *
 */
import { MenuItem } from 'primeng/api';

export const MENU: MenuItem[] = [
    {
        id: 'home',
        label: 'Home Page',
        icon: 'pi pi-home',
        routerLink: '/home'
    },
    {
        id: 'profile',
        label: 'Profile',
        icon: 'pi pi-user',
        routerLink: '/users/profile',
        state: {requiredLogin: true}
    },
    {
        id: 'categories',
        label: 'Categories',
        icon: 'pi pi-folder',
        routerLink: '/categories/all'
    },
    // {
    //    id: 'forums',
    //    label: 'Forums',
    //    icon: 'pi pi-bars'
    // },
    // {
    //     id: 'galery',
    //     label: 'Galery',
    //     icon: 'pi pi-images'
    // },
    // {
    //     id: 'downloads',
    //     label: 'Downloads',
    //     icon: 'pi pi-download'
    // },
    {
        id: 'admin',
        label: 'Administration',
        icon: 'pi pi-briefcase',
        routerLink: '/admin/home',
        state: {requiredLogin: true, group: 1}
    }
];
