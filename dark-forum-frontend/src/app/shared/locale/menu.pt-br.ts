/**
 *
 */
import { MenuItem } from 'primeng/api';

export const MENU: MenuItem[] = [
    {
        id: 'home',
        label: 'Inicio',
        icon: 'pi pi-home',
        routerLink: '/home'
    },
    {
        id: 'profile',
        label: 'Perfil',
        icon: 'pi pi-user',
        routerLink: '/users/profile',
        state: { requiredLogin: true }
    },
    {
        id: 'categories',
        label: 'Categorias',
        icon: 'pi pi-folder',
        routerLink: '/categories/all'
    },
    // {
    //    id: 'forums',
    //    label: 'Fórums',
    //    icon: 'pi pi-bars'
    // },
    // {
    //     id: 'galery',
    //     label: 'Imagens',
    //     icon: 'pi pi-images'
    // },
    // {
    //     id: 'downloads',
    //     label: 'Downloads',
    //     icon: 'pi pi-download'
    // },
    {
        id: 'admin',
        label: 'Administração',
        icon: 'pi pi-briefcase',
        routerLink: '/admin/home',
        state: { requiredLogin: true, group: 1 }
    }
];
