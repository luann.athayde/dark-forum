/**
 * Default temporary account
 */

export const DEFAULT_ACCOUNT = {
    lastLogin: '',
    lastIp: '',
    macAdress: '4A:32:90:78:6C:87:43',
    lastPincodeChange: '',
    currentIp: '',
    register: '',
    rops: null,
    vipTime: null,
    vipChoise: 1,
    userid: 'lrathayde',
    password: null,
    accountId: 2000001,
    passwordConfirmation: null,
    sex: 'M',
    email: 'lrathayde@darkfirero.net',
    groupId: 99,
    login: null,
    birthdate: '2020-06-02T03:00:00.000Z',
    birthdateISO: '2020-06-02T00:00:00.000Z',
    chars: []
};

