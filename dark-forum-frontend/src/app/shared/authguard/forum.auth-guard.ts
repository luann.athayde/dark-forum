/**
 *
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app.component';
import { LanguageManager } from '../locale/language-manager';

/**
 *
 */
@Injectable()
export class ForumAuthGuard implements CanActivate, CanActivateChild {

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return new Promise((resolve, reject) => {
            resolve(true);
            this.updateBreadCrumb(route);
        });
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return new Promise((resolve, reject) => {
            resolve(true);
            this.updateBreadCrumb(childRoute);
        });
    }

    updateBreadCrumb(route: ActivatedRouteSnapshot) {
        AppComponent.breadcrumbEvent.emit(this.createBreadcrumb(route));
    }

    createBreadcrumb(route: ActivatedRouteSnapshot) {
        const breadcrumb = [
            {
                label: LanguageManager.getInstance().getLanguage().getHomePage(),
                icon: 'pi pi-home',
                routerLink: '/home'
            }
        ];

        if (route && route.data && route.data.id) {
            const menuItem = LanguageManager.getInstance().getLanguage().listMenu().find(item => {
                return item.id === route.data.id;
            });
            if (menuItem) {
                breadcrumb.push({
                    label: menuItem.label,
                    icon: menuItem.icon,
                    routerLink: menuItem.routerLink
                });
            }
            const slug = route.paramMap.get('slug');
            if (slug) {
                breadcrumb.push({
                    label: slug,
                    icon: route.data.icon,
                    routerLink: route.data.link
                });
            }
        }

        return breadcrumb;
    }

}

