import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HomepageModule } from './modules/homepage/homepage.module';
import { TopicsModule } from './modules/topics/topics.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ForumAuthGuard } from './shared/authguard/forum.auth-guard';
import { HttpClientModule } from '@angular/common/http';
import { MenubarModule } from 'primeng/menubar';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { CardModule } from 'primeng/card';
import { SplitButtonModule } from 'primeng/splitbutton';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PasswordModule } from 'primeng/password';
import { BlockUIModule } from 'primeng/blockui';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, MessageService } from 'primeng/api';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        MenubarModule,
        InputTextModule,
        ButtonModule,
        BreadcrumbModule,
        CardModule,
        SplitButtonModule,
        BrowserAnimationsModule,
        OverlayPanelModule,
        PasswordModule,
        BlockUIModule,
        ProgressSpinnerModule,
        TooltipModule,
        ConfirmDialogModule,
        HomepageModule,
        TopicsModule
    ],
    providers: [
        ForumAuthGuard,
        MessageService,
        ConfirmationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
