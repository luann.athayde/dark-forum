-- --
-- Database
drop database if exists forum;
create database forum CHARSET = UTF8 collate = utf8_bin;
-- --
-- Tables

create table forum.group
(
    id          integer      not null auto_increment primary key,
    name        varchar(256) not null,
    description varchar(512) not null,
    create_date timestamp    not null default CURRENT_TIMESTAMP,
    delete_date datetime     null,
    index group_name (name)
) ENGINE = MyISAM;
insert into forum.group
values (1, 'admin', 'Administrador', CURRENT_TIMESTAMP, null);
insert into forum.group
values (2, 'moderador', 'Moderador', CURRENT_TIMESTAMP, null);
insert into forum.group
values (3, 'user', 'Usuário', CURRENT_TIMESTAMP, null);

create table forum.user
(
    id              integer       not null auto_increment primary key,
    group_id        integer       not null default 0,
    email           varchar(256)  not null,
    name            varchar(128)  not null,
    birthday        datetime      not null,
    bio             varchar(2048) null,
    profile_picture varchar(2048) null,
    userid          varchar(128)  not null,
    password        varchar(256)  null,
    login_id        integer       null,
    online          boolean       default false,
    create_date     timestamp     not null default CURRENT_TIMESTAMP,
    delete_date     datetime      null,
    foreign key user_group_fk (group_id)
        references forum.group (id) on delete RESTRICT
) ENGINE = MyISAM;
insert into forum.user
values (1, 1, 'admin@admin.com', 'Administrador', '20200101',
        null, null, 'admin', md5('admin'), null, false, CURRENT_TIMESTAMP, null);

create table forum.tag
(
    id          integer     not null auto_increment primary key,
    name        varchar(64) not null,
    user_id     integer     not null,
    create_date timestamp   not null default CURRENT_TIMESTAMP,
    delete_date datetime    null,
    foreign key tag_user_fk (user_id)
        references forum.user (id) on delete RESTRICT
) ENGINE = MyISAM;
insert into forum.tag
values (1, 'Facebook', 1, CURRENT_TIMESTAMP, null);
insert into forum.tag
values (2, 'Ragnarok RE', 1, CURRENT_TIMESTAMP, null);
insert into forum.tag
values (3, 'Comércio', 1, CURRENT_TIMESTAMP, null);
insert into forum.tag
values (4, 'Builds', 1, CURRENT_TIMESTAMP, null);
insert into forum.tag
values (5, 'VIP', 1, CURRENT_TIMESTAMP, null);

create table forum.category
(
    id          integer      not null auto_increment primary key,
    name        varchar(256) not null,
    slug        varchar(48)  not null,
    pinned      boolean               default false,
    user_id     integer      not null,
    group_id    integer      not null,
    create_date timestamp    not null default CURRENT_TIMESTAMP,
    delete_date datetime     null,
    index category_name (name),
    index category_slug (slug),
    foreign key category_user_fk (user_id)
        references forum.user (id) on delete RESTRICT,
    foreign key category_group_fk (group_id)
        references forum.group (id) on delete RESTRICT
) ENGINE = MyISAM;

create table forum.forum
(
    id           integer      not null auto_increment primary key,
    category_id  integer      not null,
    slug         varchar(48)  not null,
    name         varchar(256) not null,
    description  varchar(512) not null,
    pinned       boolean      not null default false,
    locked       boolean      not null default false,
    total_topics integer      not null default 0,
    user_id      integer      not null,
    group_id     integer      not null,
    create_date  timestamp    not null default CURRENT_TIMESTAMP,
    delete_date  datetime     null,
    index forum_id (id),
    index forum_category_id (category_id),
    index forum_name (name),
    index forum_slug (slug),
    foreign key forum_category_fk (category_id)
        references forum.category (id) on delete RESTRICT,
    foreign key forum_user_fk (user_id)
        references forum.user (id) on delete RESTRICT,
    foreign key forum_group_fk (group_id)
        references forum.group (id) on delete RESTRICT
) ENGINE = MyISAM;

create table forum.topic
(
    id          integer      not null auto_increment primary key,
    forum_id    integer      not null,
    slug        varchar(48)  not null,
    name        varchar(256) not null,
    body        text         not null,
    views       integer(22)  not null default 0,
    locked      boolean      not null default false,
    user_id     integer      not null,
    edit_date   datetime     null,
    create_date timestamp    not null default CURRENT_TIMESTAMP,
    delete_date datetime     null,
    index topic_id (id),
    index topic_forum_id (forum_id),
    index topic_name (name),
    index topic_slug (slug),
    foreign key topic_forum_fk (forum_id)
        references forum.forum (id) on delete RESTRICT,
    foreign key topic_user_fk (user_id)
        references forum.user (id) on delete RESTRICT
) ENGINE = MyISAM;

create table forum.topic_tags
(
    id          integer   not null auto_increment primary key,
    tag_id      integer   not null,
    topic_id    integer   not null,
    user_id     integer   not null,
    create_date timestamp not null default CURRENT_TIMESTAMP,
    delete_date datetime  null,
    index topic_tags_id (id),
    index topic_tags_tag_id (tag_id),
    index topic_tags_topic_id (topic_id),
    foreign key topic_tags_tag_fk (tag_id)
        references forum.tag (id) on delete RESTRICT,
    foreign key topic_tags_topic_fk (topic_id)
        references forum.topic (id) on delete RESTRICT,
    foreign key topic_tags_user_fk (user_id)
        references forum.user (id) on delete RESTRICT
) ENGINE = MyISAM;

create table forum.reply
(
    id          integer   not null auto_increment primary key,
    topic_id    integer   not null,
    body        text      not null,
    user_id     integer   not null,
    create_date timestamp not null default CURRENT_TIMESTAMP,
    delete_date datetime  null,
    index reply_id (id),
    index reply_category_fk (topic_id),
    foreign key reply_topic_fk (topic_id)
        references forum.topic (id) on delete RESTRICT,
    foreign key reply_user_fk (user_id)
        references forum.user (id) on delete RESTRICT
) ENGINE = MyISAM;


create table forum.event
(
    id          integer       not null auto_increment primary key,
    topic_id    integer       not null,
    name        varchar(256)  not null,
    description text          not null,
    banner      varchar(1024) null,
    event_date  datetime      not null,
    user_id     integer       not null,
    create_date timestamp     not null default CURRENT_TIMESTAMP,
    delete_date datetime      null,
    index reply_id (id),
    index reply_category_fk (topic_id),
    foreign key reply_topic_fk (topic_id)
        references forum.topic (id) on delete RESTRICT,
    foreign key reply_user_fk (user_id)
        references forum.user (id) on delete RESTRICT
) ENGINE = MyISAM;


