package net.darkforum.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 */
@Data
@MappedSuperclass
public abstract class BasicEntity implements Serializable {

    @Column(name = "create_date")
    protected LocalDateTime createDate;

    @Column(name = "delete_date")
    protected LocalDateTime deleteDate;

}
