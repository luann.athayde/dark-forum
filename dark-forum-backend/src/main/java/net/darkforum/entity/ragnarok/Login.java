/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.darkforum.entity.ragnarok;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author luann
 */
@Data
@Entity
@Table(name = "login")
public class Login {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Long accountId;

    @Size(min = 1, max = 23)
    @Column(name = "userid")
    private String userid;

    @Size(min = 1, max = 32)
    @Column(name = "user_pass")
    private String password;

    @Size(min = 1, max = 2)
    @Column(name = "sex")
    private String sex;

    @Size(min = 1, max = 39)
    @Column(name = "email")
    private String email;

    @Column(name = "group_id")
    private Long groupId;

    @Column(name = "state")
    private Integer state;

    @Column(name = "unban_time")
    private Integer unbanTime;

    @Column(name = "expiration_time")
    private Integer expirationTime;

    @Column(name = "logincount")
    private Integer logincount;

    @Column(name = "lastlogin")
    private LocalDateTime lastlogin;

    @Size(min = 1, max = 100)
    @Column(name = "last_ip")
    private String lastIp;

    @Column(name = "birthdate")
    private LocalDate birthdate;

    @Column(name = "character_slots", nullable = false)
    private Short characterSlots;

    @Column(name = "pincode", nullable = false)
    private String pincode;

    @Column(name = "pincode_change")
    private Long pincodeChange;

    @Column(name = "vip_time")
    private Long vipTime;

    @Column(name = "old_group")
    private Long oldGroup;

    @Column(name = "register_date")
    private LocalDateTime registerDate;

    @Column(name = "change_password")
    private boolean changePassword;

}
