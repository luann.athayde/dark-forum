package net.darkforum.constants;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 *
 */
@Getter
public enum Configuration {

    // ---
    RAGNAROK_LOGIN_INTEGRATION("dark-forum.user-ragnarok-login", false, "true"),
    USER_GROUP("dark-forum.user-group", false, "3"),
    // ---
    ;

    private final String propertyName;
    private final Boolean required;
    private final String defaultValue;

    Configuration(String propertyName, Boolean required, String defaultValue) {
        this.propertyName = propertyName;
        this.required = required;
        this.defaultValue = defaultValue;
    }

    public String getValue() {
        if (required) {
            return System.getProperty(propertyName);
        } else {
            var propertyValue = System.getProperty(propertyName);
            if (StringUtils.isNotEmpty(propertyValue)) {
                return propertyValue;
            }
        }
        return defaultValue;
    }

    public Boolean getValueAsBoolean() {
        return Boolean.valueOf(getValue());
    }

    public Long getValueAsLong() {
        return Long.valueOf(getValue());
    }

}

