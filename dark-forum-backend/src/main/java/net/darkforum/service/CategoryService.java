package net.darkforum.service;

import lombok.extern.slf4j.Slf4j;
import net.darkforum.dao.forum.CategoryDAO;
import net.darkforum.dao.forum.ForumDAO;
import net.darkforum.dto.forum.CategoryDTO;
import net.darkforum.dto.forum.CategoryFilterDTO;
import net.darkforum.dto.forum.ForumDTO;
import net.darkforum.dto.forum.ForumFilterDTO;
import net.darkforum.entity.forum.Category;
import net.darkforum.entity.forum.User;
import net.darkforum.exception.ValidationException;
import net.darkforum.parser.forum.CategoryParser;
import net.darkforum.parser.forum.ForumParser;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 */
@Slf4j
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CategoryService extends Service {

    @Inject
    protected CategoryDAO categoryDAO;
    @Inject
    protected ForumDAO forumDAO;

    public List<CategoryDTO> listCategories(CategoryFilterDTO filter) {

        List<Category> categories = categoryDAO.listAll(filter);

        return categories.stream().map(
                category -> CategoryParser.get().toDto(category)
        ).collect(Collectors.toList());
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public CategoryDTO save(CategoryDTO categoryDTO) {

        Category category = CategoryParser.get().toEntity(categoryDTO);
        category.setId(null);
        category.setCreateDate(LocalDateTime.now());
        if(Objects.isNull(categoryDTO.getPinned())) {
            category.setPinned(Boolean.FALSE);
        }

        category.setUser(
                userDAO.findByPk(category.getUser().getId())
        );
        category.setGroup(
                groupDAO.findByPk(category.getGroup().getId())
        );

        return CategoryParser.get().toDto(
                categoryDAO.save(category)
        );
    }

    public CategoryDTO findBySlug(String slug) {

        Category category = categoryDAO.findBySlug(slug);

        CategoryDTO categoryDTO = CategoryParser.get().toDto(category);

//        categoryDTO.setForums(
//                forumDAO.listByFilter(
//                        ForumFilterDTO.builder().idCategory(category.getId()).build()
//                ).stream().map(
//                        forum -> ForumParser.get().toDto(forum)
//                ).collect(Collectors.toList())
//        );

        return categoryDTO;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public CategoryDTO delete(String slug, Long userId) throws ValidationException {

        User user = userDAO.findByPk( userId );
        Category category = categoryDAO.findBySlug(slug);

        Long userGroup = user.getGroup().getId();
        Long categoryGroup = category.getGroup().getId();
        if( userGroup.equals(categoryGroup) ) {
            category.setDeleteDate(LocalDateTime.now());
            category.setUser(user);
            return CategoryParser.get().toDto(
                    categoryDAO.save(category)
            );
        }

        throw new ValidationException(403, "Invalid user/group for category deletion.");
    }

}
