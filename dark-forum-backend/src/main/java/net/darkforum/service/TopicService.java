package net.darkforum.service;

import lombok.extern.slf4j.Slf4j;
import net.darkforum.dao.forum.*;
import net.darkforum.dto.forum.*;
import net.darkforum.entity.forum.*;
import net.darkforum.parser.forum.*;
import org.apache.commons.lang3.tuple.Pair;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

/**
 *
 */
@Slf4j
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class TopicService extends Service {

    @Inject
    protected ForumDAO forumDAO;
    @Inject
    protected TopicDAO topicDAO;
    @Inject
    protected TopicTagDAO topicTagDAO;
    @Inject
    protected TagDAO tagDAO;

    public Pair<List<TopicDTO>, Long> listTopics(TopicFilterDTO filter) {

        Long totalTopics = topicDAO.totalByFilter(filter);
        List<Topic> topics = topicDAO.listByFilter(filter);

        List<TopicDTO> topicsDTO = topics.stream().map(
                topic -> TopicDTO.builder()
                        .id(topic.getId())
                        .slug(topic.getSlug())
                        .locked(topic.getLocked())
                        .name(topic.getName())
                        .views(topic.getViews())
                        .tags(new ArrayList<>())
                        .rating(5L)
                        .createDate(topic.getCreateDate())
                        .user(UserParser.get().toDto(topic.getUser()))
                        .build()
        ).collect(Collectors.toList());

        return Pair.of( topicsDTO, totalTopics );
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TopicDTO save(TopicDTO topicDTO) {

        Topic topic;
        Forum forum = forumDAO.findByPk(topicDTO.getForum().getId());
        User user = userDAO.findByPk(topicDTO.getUser().getId());
        List<TagDTO> tags = topicDTO.getTags().stream().map(TopicTagDTO::getTag).collect(Collectors.toList());

        if (Objects.nonNull(topicDTO.getId())) {
            topic = topicDAO.findByPk(topicDTO.getId());
            topic.setBody(topicDTO.getBody());
            topic.setLocked(Boolean.TRUE.equals(topicDTO.getLocked()));
            // ---
            List<TopicTag> removes = topic.getTags().stream().filter(
                    topicTag -> tags.stream().noneMatch(tag -> topicTag.getTag().getName().equals(tag.getName()))
            ).collect(Collectors.toList());
            topic.getTags().removeAll(removes);
            // ---
        } else {
            topic = TopicParser.get().toEntity(topicDTO);
            topic.setTags(new ArrayList<>());
            forum.setTotalTopics(forum.getTotalTopics() + 1L);
            forumDAO.save(forum);
        }
        topic.setForum(forum);
        topic.setUser(user);
        topicDAO.save(topic);

        List<TopicTag> topicTags = topic.getTags();
        tags.stream().filter(tagDTO ->
                topicTags.stream().noneMatch(topicTag -> topicTag.getTag().getName().equals(tagDTO.getName()))
        ).map(tagDTO -> TagParser.get().toEntity(tagDTO)).forEach(tag -> {
            topicTags.add(createTopicTag(topic, tag, user));
        });

        return TopicParser.get().toDto(topic);
    }

    private TopicTag createTopicTag(Topic topic, Tag tag, User user) {
        TopicTag topicTag = new TopicTag();
        topicTag.setCreateDate(LocalDateTime.now());
        topicTag.setUser(user);
        topicTag.setTopic(topic);
        try {
            tag = tagDAO.findByName(tag.getName());
        } catch (NoResultException e) {
            tag.setUser(user);
            tag.setCreateDate(LocalDateTime.now());
            tag = tagDAO.save(tag);
        }
        topicTag.setTag(tag);
        return topicTag;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TopicDTO findBySlug(String slug) {

        Topic topic = topicDAO.findBySlug(slug);
        topic.setViews(topic.getViews() + 1L);
        TopicDTO topicDTO = TopicParser.get().toDto(
                topic
        );
        topicDTO.getTags().forEach(topicTagDTO -> {
            topicTagDTO.setTopic(null);
        });
        topicDTO.setRating(5L);

        return topicDTO;
    }

}
