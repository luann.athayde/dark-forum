package net.darkforum.service;

import lombok.extern.slf4j.Slf4j;
import net.darkforum.constants.Configuration;
import net.darkforum.dao.ragnarok.LoginDAO;
import net.darkforum.dto.forum.UserDTO;
import net.darkforum.dto.ragnarok.LoginDTO;
import net.darkforum.entity.forum.User;
import net.darkforum.exception.ValidationException;
import net.darkforum.parser.forum.UserParser;
import net.darkforum.parser.ragnarok.LoginParser;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.core5.http.HttpStatus;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
@Slf4j
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class UserService extends Service {

    @Inject
    protected UserService self;

    @Inject
    protected LoginDAO loginDAO;

    /**
     * @param id
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public UserDTO findUserById(Long id) {
        return UserParser.get().toDto(
                userDAO.findByPk(id)
        );
    }

    /**
     * @param userDTO
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public UserDTO updateUser(UserDTO userDTO) {

        var user = userDAO.findByPk(userDTO.getId());
        user.setOnline(Boolean.TRUE.equals(userDTO.getOnline()));

        return UserParser.get().toDto(
                userDAO.save(user)
        );
    }

    /**
     * @param userid
     * @param password
     * @return
     */
    public UserDTO findByUserAndPassword(String userid, String password)
            throws ValidationException {
        // ---
        if (Configuration.RAGNAROK_LOGIN_INTEGRATION.getValueAsBoolean()) {
            try {
                var login = loginDAO.findByUserAndPassword(userid, password);

                return userByRagnarokAccount(LoginParser.get().toDto(login));
            } catch (Exception e) {
                log.error("Fail to find user with ragnarok database: {}", e, e);
                throw new ValidationException(HttpStatus.SC_UNAUTHORIZED, "Usuário/senha inválidos!");
            }
        }
        return userByForumAccount(userid, password);
        // ---
    }

    /**
     * @param userid
     * @param password
     * @return
     * @throws ValidationException
     */
    public UserDTO userByForumAccount(String userid, String password)
            throws ValidationException {
        try {
            var user = userDAO.findByUserAndPassword(userid, password);

            return UserParser.get().toDto(user);
        } catch (Exception e) {
            log.error("Fail to find user with forum database: {}", e, e);
        }
        // ---
        throw new ValidationException(HttpStatus.SC_UNAUTHORIZED, "Usuário/senha inválidos!");
    }

    /**
     * @param loginDTO
     * @return
     */
    public UserDTO userByRagnarokAccount(LoginDTO loginDTO) {
        try {
            var user = userDAO.findByEmail(loginDTO.getEmail());

            return UserParser.get().toDto(user);
        } catch (NoResultException nre) {
            log.warn("No user for ragnarok account with login [{}]...", loginDTO.getEmail());
        }
        return self.createUserByRagnarokAccount(loginDTO);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public UserDTO createUserByRagnarokAccount(LoginDTO loginDTO) {
        var group = groupDAO.findByPk(Configuration.USER_GROUP.getValueAsLong()); // Normal user
        var user = new User();
        user.setGroup(group);
        user.setBirthday(loginDTO.getBirthdate());
        user.setName(loginDTO.getUserid());
        user.setUserid(loginDTO.getUserid());
        user.setEmail(loginDTO.getEmail());
        user.setBio(StringUtils.EMPTY);
        user.setLoginId(loginDTO.getAccountId());
        user.setCreateDate(LocalDateTime.now());
        user.setOnline(Boolean.FALSE);
        // ---
        return UserParser.get().toDto(
                userDAO.save(user)
        );
    }

    public List<UserDTO> getOnlineUsers() {
        return userDAO.getOnlineUsers().stream().map(
                user -> UserDTO.builder()
                        .name(user.getName())
                        .online(user.getOnline())
                        .userid(user.getUserid())
                        .build()
        ).collect(Collectors.toList());
    }

}
