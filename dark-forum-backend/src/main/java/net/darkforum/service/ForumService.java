package net.darkforum.service;

import lombok.extern.slf4j.Slf4j;
import net.darkforum.dao.forum.*;
import net.darkforum.dto.forum.*;
import net.darkforum.entity.forum.Forum;
import net.darkforum.entity.forum.Topic;
import net.darkforum.parser.forum.ForumParser;
import net.darkforum.parser.forum.TopicParser;
import net.darkforum.parser.forum.UserParser;
import org.apache.commons.lang3.tuple.Pair;

import javax.ejb.AsyncResult;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 *
 */
@Slf4j
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ForumService extends Service {

    @Inject
    protected ForumService self;

    @Inject
    protected CategoryDAO categoryDAO;
    @Inject
    protected ForumDAO forumDAO;
    @Inject
    protected TopicDAO topicDAO;

    public Pair<List<ForumDTO>, Long> listForums(ForumFilterDTO filter) {

        Long total = forumDAO.totalByFilter(filter);
        List<Forum> forums = forumDAO.listByFilter(filter);

        List<ForumDTO> forumsDTO = forums.stream().map(
                forum -> ForumParser.get().toDto(forum)
        ).collect(Collectors.toList());

        return Pair.of( forumsDTO, total );
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public ForumDTO save(ForumDTO forumDTO) {

        Forum forum = ForumParser.get().toEntity(forumDTO);
        if (Objects.nonNull(forum.getId())) {
            forum = forumDAO.findByPk(forum.getId());
            forum.setDescription(forumDTO.getDescription());
            forum.setLocked(Boolean.TRUE.equals(forumDTO.getLocked()));
            forum.setPinned(Boolean.TRUE.equals(forumDTO.getPinned()));
        } else {
            forum.setCategory(
                    categoryDAO.findByPk(forumDTO.getCategory().getId())
            );
            forum.setGroup(
                    groupDAO.findByPk(forum.getGroup().getId())
            );
        }
        // ---
        forum.setUser(
                userDAO.findByPk(forum.getUser().getId())
        );

        // ---
        return ForumParser.get().toDto(
                forumDAO.save(forum)
        );
    }

    public ForumDTO findBySlug(String slug) {

        ForumDTO forumDTO = ForumParser.get().toDto(
                forumDAO.findBySlug(slug)
        );
        forumDTO.setCategory( CategoryDTO.builder().id(forumDTO.getCategory().getId()).build() );
        forumDTO.setUser( UserDTO.builder().id(forumDTO.getUser().getId()).build() );

        self.updateForumViews(forumDTO.getId());

        return forumDTO;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Future<Long> updateForumViews(Long forumId) {

        Forum forum = forumDAO.findByPk( forumId );
        forum.setTotalViews( forum.getTotalViews() + 1L);
        forum = forumDAO.save(forum);

        return new AsyncResult<>( forum.getTotalViews() );
    }

}
