package net.darkforum.service;

import lombok.Data;
import net.darkforum.dao.forum.GroupDAO;
import net.darkforum.dao.forum.UserDAO;

import javax.inject.Inject;
import java.io.Serializable;

/**
 *
 */
@Data
public class Service implements Serializable {

    @Inject
    protected GroupDAO groupDAO;
    @Inject
    protected UserDAO userDAO;

}
