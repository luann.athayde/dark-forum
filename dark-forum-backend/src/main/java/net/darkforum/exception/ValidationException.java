package net.darkforum.exception;

import lombok.Data;

@Data
public class ValidationException extends Exception {

    private int status;
    private String message;

    public ValidationException(String message) {
        super(message);
        this.status = 500;
        this.message = message;
    }

    public ValidationException(int status, String message) {
        super(message);
        this.status = status;
        this.message = message;
    }

}
