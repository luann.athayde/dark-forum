package net.darkforum.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Slf4j
public class LocalDateSerializer extends JsonSerializer<LocalDate> {

    @Override
    public void serialize(LocalDate localDate, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider)
            throws IOException {
        if (Objects.nonNull(localDate)) {
            try {
                jsonGenerator.writeString(DateUtils.convertToISO(localDate));
            } catch (Exception e) {
                log.error("Fail to serialize LocalDateTime!", e);
                jsonGenerator.writeNull();
            }
        } else {
            jsonGenerator.writeNull();
        }
    }

}
