package net.darkforum.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Objects;

@Slf4j
public class LocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {

    @Override
    public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider)
            throws IOException {
        if (Objects.nonNull(localDateTime)) {
            try {
                jsonGenerator.writeString(DateUtils.convertToISO(localDateTime));
            } catch (Exception e) {
                log.error("Fail to serialize LocalDateTime!", e);
                jsonGenerator.writeNull();
            }
        } else {
            jsonGenerator.writeNull();
        }
    }

}
