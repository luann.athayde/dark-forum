package net.darkforum.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Image Utils class
 */
@Slf4j
public class MediaUtils {

    private MediaUtils() {
        throw new IllegalStateException("Classe Utilitaria.");
    }

    /**
     * @param file File to be converted to base64.
     * @return base64 data
     */
    public static String convertFileToBase64(File file) {
        // ---
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            return MediaUtils.convertFileToBase64(fileInputStream, (int) file.length());
        } catch (Exception e) {
            log.error("Falha ao converter arquivo para Base64! | ERROR = " + e.getMessage());
        }
        return "";
    }

    /**
     * @param fileInputStream Data to be converted to base64.
     * @return base64 data
     */
    public static String convertFileToBase64(InputStream fileInputStream, int size) {
        // ---
        return MediaUtils.createBase64Image(
                MediaUtils.encodeDataToBase64(fileInputStream, size)
        );
    }

    /**
     * @param inputStream Data to be converted into BASE64 (ex: BMP/PNG)
     * @return base64 Image
     */
    public static String encodeDataToBase64(InputStream inputStream, int size) {
        try {
            byte[] buffer = new byte[size];
            int read = inputStream.read(buffer, 0, size);
            if (read == buffer.length) {
                return new String(Base64.encodeBase64(buffer), StandardCharsets.UTF_8);
            }
        } catch (Exception e) {
            log.error("Falha ao converter arquivo para Base64! | ERROR = " + e.getMessage());
        }
        return null;
    }

    /**
     * @param dataBase64 Data in base64 enconding.
     * @return image/base64 format
     */
    public static String createBase64Image(String dataBase64) {
        StringBuilder fileImageCode64Parameter = new StringBuilder();
        fileImageCode64Parameter.append("data:image/");
        fileImageCode64Parameter.append("jpeg;");
        fileImageCode64Parameter.append("base64,");
        if (StringUtils.isNotEmpty(dataBase64)) {
            fileImageCode64Parameter.append(dataBase64);
        }
        return fileImageCode64Parameter.toString();
    }

    /**
     * @param nomeArquivo
     * @return
     */
    public static String getFileExtension(String nomeArquivo) {
        try {
            return nomeArquivo.substring(nomeArquivo.lastIndexOf('.') + 1);
        } catch (Exception e) {
            return "";
        }
    }

}
