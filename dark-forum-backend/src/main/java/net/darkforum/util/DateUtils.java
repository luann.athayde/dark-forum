/**
 * DateUtils Class...
 */
package net.darkforum.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Luann Athayde
 */
public class DateUtils {

    public static LocalDateTime convertFromISO(String isoDateTime) {
        return LocalDateTime.parse(
                isoDateTime, DateTimeFormatter.ISO_DATE_TIME
        );
    }

    public static String convertToISO(LocalDateTime dateTime) {
        return DateTimeFormatter.ISO_DATE_TIME.format(dateTime);
    }

    public static String convertToISO(LocalDate dateTime) {
        return DateTimeFormatter.ISO_DATE_TIME.format(dateTime.atStartOfDay());
    }

}
