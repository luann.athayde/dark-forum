package net.darkforum.rest;

import net.darkforum.dto.forum.TopicDTO;
import net.darkforum.dto.forum.TopicFilterDTO;
import net.darkforum.service.TopicService;
import org.apache.commons.lang3.tuple.Pair;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Stateless
@Path("topic")
public class TopicRest extends DarkForumRest {

    @Inject
    private TopicService topicService;

    @GET
    public Response listTopics(@BeanParam TopicFilterDTO filter) {
        try {
            Pair<List<TopicDTO>, Long> listData = topicService.listTopics(filter);
            // ---
            Map<String, Object> data = new HashMap<>();
            data.put("list", listData.getLeft());
            data.put("total", listData.getRight());

            return Response.ok(data).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @POST
    public Response save(TopicDTO dto) {
        try {
            dto = topicService.save(dto);
            // ---
            return Response.ok(dto).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @GET
    @Path("{slug}")
    public Response findBySlug(@PathParam("slug") String slug) {
        try {
            TopicDTO dto = topicService.findBySlug(slug);
            // ---
            return Response.ok(dto).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

}
