package net.darkforum.rest;

import net.darkforum.dto.forum.ForumDTO;
import net.darkforum.dto.forum.ForumFilterDTO;
import net.darkforum.service.ForumService;
import org.apache.commons.lang3.tuple.Pair;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Stateless
@Path("forum")
public class ForumRest extends DarkForumRest {

    @Inject
    private ForumService forumService;

    @GET
    public Response listForums(@BeanParam ForumFilterDTO filter) {
        try {
            Pair<List<ForumDTO>, Long> listData = forumService.listForums(filter);
            // ---
            Map<String, Object> data = new HashMap<>();
            data.put("list", listData.getLeft());
            data.put("total", listData.getRight());

            return Response.ok(data).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @POST
    public Response save(ForumDTO dto) {
        try {
            dto = forumService.save(dto);
            // ---
            return Response.ok(dto).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @GET
    @Path("{slug}")
    public Response findBySlug(@PathParam("slug") String slug) {
        try {
            ForumDTO dto = forumService.findBySlug(slug);
            // ---
            return Response.ok(dto).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

}
