package net.darkforum.rest;

import net.darkforum.dto.forum.UserDTO;
import net.darkforum.dto.ragnarok.LoginDTO;
import net.darkforum.service.UserService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 *
 */
@Stateless
@Path("user")
public class UserRest extends DarkForumRest {

    @EJB
    private UserService userService;

    @POST
    @Path("id/{id}")
    public Response findById(@PathParam("id") Long id) {
        try {
            UserDTO dto = userService.findUserById(id);
            // ---
            return Response.ok(dto).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @GET
    @Path("login")
    public Response findByUserAndPassword(
            @QueryParam("userid") String userid,
            @QueryParam("password") String password
    ) {
        try {
            UserDTO userDTO = userService.findByUserAndPassword(userid, password);

            return Response.ok(userDTO).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @POST
    @Path("ragnarok/account")
    public Response userByRagnarokAccount(LoginDTO loginDTO) {
        try {
            UserDTO userDTO = userService.userByRagnarokAccount(loginDTO);

            return Response.ok(userDTO).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @GET
    @Path("online")
    public Response getOnlineUsers() {
        try {
            List<UserDTO> onlineUsers = userService.getOnlineUsers();

            return Response.ok(onlineUsers).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @POST
    public Response updateUser(UserDTO userDTO) {
        try {
            userDTO = userService.updateUser(userDTO);

            return Response.ok(userDTO).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

}
