package net.darkforum.rest;

import net.darkforum.dto.forum.CategoryDTO;
import net.darkforum.dto.forum.CategoryFilterDTO;
import net.darkforum.dto.forum.UserDTO;
import net.darkforum.service.CategoryService;
import net.darkforum.service.UserService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Stateless
@Path("category")
public class CategoryRest extends DarkForumRest {

    @Inject
    private CategoryService categoryService;

    @GET
    public Response listCategories(@BeanParam CategoryFilterDTO filter) {
        try {
            List<CategoryDTO> categories = categoryService.listCategories(filter);
            // ---
            Map<String, Object> data = new HashMap<>();
            data.put("list", categories);
            data.put("total", categories.size());

            return Response.ok(data).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @GET
    @Path("{slug}")
    public Response findBySlug(@PathParam("slug") String slug) {
        try {
            CategoryDTO category = categoryService.findBySlug(slug);
            // ---
            return Response.ok(category).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @POST
    public Response save(CategoryDTO category) {
        try {
            category = categoryService.save( category );
            // ---
            return Response.ok(category).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

    @DELETE
    @Path("{slug}")
    public Response delete( @PathParam("slug") String slug, @QueryParam("userId") Long userId ) {
        try {
            CategoryDTO categoryDTO = categoryService.delete( slug, userId );
            // ---
            return Response.ok(categoryDTO).build();
        } catch (Exception e) {
            return processError(e);
        }
    }

}
