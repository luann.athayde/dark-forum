package net.darkforum.rest;

import lombok.extern.slf4j.Slf4j;
import net.darkforum.dto.MessageDTO;
import net.darkforum.exception.ValidationException;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.Optional;

/**
 * @author Luann Athayde
 * @version 1.0.0
 */
@Slf4j
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public abstract class DarkForumRest implements Serializable {

    public Response processError(Throwable e) {

        int status = 500;
        String message = "Falha ao realizar operação, tente novamente mais tarde.";
        String stackTrace = "-";

        if (e instanceof ValidationException) {
            ValidationException ve = (ValidationException) e;
            status = ve.getStatus();
            message = ve.getMessage();
        }

        MessageDTO messageDTO = MessageDTO.builder()
                .message(message)
                .status(status)
                .stackTrace(stackTrace)
                .build();

        return Response
                .status(messageDTO.getStatus())
                .entity(messageDTO)
                .build();
    }

    public Optional<HttpServletRequest> getRequest() {
        try {
            return Optional.of(
                    ResteasyProviderFactory.getInstance().getContextData(HttpServletRequest.class)
            );
        } catch (Exception e) {
            log.warn("### Fail to obtain HTTP Request Context: {}", e, e);
        }
        return Optional.empty();
    }

    public String getMacAdress() {
        var requestContext = getRequest();
        if (requestContext.isPresent()) {
            return requestContext.get().getRemoteUser();
        }
        return "-";
    }

    public String getRequestIp() {
        var requestContext = getRequest();
        if (requestContext.isPresent()) {
            return requestContext.get().getRemoteAddr();
        }
        return "-";
    }

}
