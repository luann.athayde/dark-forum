package net.darkforum.dao.forum;

import net.darkforum.dao.BasicDAO;
import net.darkforum.entity.forum.User;

import java.util.List;

/**
 *
 */
public class UserDAO extends BasicDAO<User> {

    public User findByUserAndPassword(String userid, String password) {
        return getEntityManager().createQuery(
                " select u from User u WHERE u.userid = :userid and u.password = :password ",
                User.class
        ).setParameter("userid", userid)
                .setParameter("password", password)
                .getSingleResult();
    }

    public User findByEmail(String email) {
        return getEntityManager().createQuery(
                " select u from User u where u.email = :email ",
                User.class
        ).setParameter("email", email).getSingleResult();
    }

    public List<User> getOnlineUsers() {
        return getEntityManager().createQuery(
                "select u from User u where u.online = :online",
                User.class
        ).setParameter(
                "online", Boolean.TRUE
        ).getResultList();
    }

}
