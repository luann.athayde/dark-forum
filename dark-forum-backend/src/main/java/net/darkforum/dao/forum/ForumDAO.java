package net.darkforum.dao.forum;

import net.darkforum.dao.BasicDAO;
import net.darkforum.dto.forum.ForumFilterDTO;
import net.darkforum.entity.forum.Forum;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Objects;

/**
 *
 */
public class ForumDAO extends BasicDAO<Forum> {

    public List<Forum> listByFilter(ForumFilterDTO filter) {

        TypedQuery<Forum> query = getEntityManager().createQuery(
                createFilteredSql(false),
                Forum.class
        ).setParameter("idCategory", filter.getIdCategory());

        if (Objects.nonNull(filter.getFirst())) {
            query.setFirstResult(filter.getFirst());
        }
        if (Objects.nonNull(filter.getRows())) {
            query.setMaxResults(filter.getRows());
        }

        return query.getResultList();
    }

    public Long totalByFilter(ForumFilterDTO filter) {

        TypedQuery<Long> query = getEntityManager().createQuery(
                createFilteredSql(true),
                Long.class
        ).setParameter("idCategory", filter.getIdCategory());

        return query.getSingleResult();
    }

    private String createFilteredSql(boolean count) {
        StringBuilder sql = new StringBuilder();
        if (count) {
            sql.append("SELECT count(f.id) FROM Forum f ");
        } else {
            sql.append("SELECT f FROM Forum f ");
        }

        sql.append(" WHERE f.category.id = :idCategory AND ");
        sql.append(" f.deleteDate IS NULL ");
        sql.append(" ORDER BY f.locked DESC, f.createDate DESC ");

        return sql.toString();
    }

    public Forum findBySlug(String slug) {
        return getEntityManager().createQuery(
                " SELECT f FROM Forum f WHERE f.slug = :slug ",
                Forum.class
        ).setParameter("slug", slug).getSingleResult();
    }

}
