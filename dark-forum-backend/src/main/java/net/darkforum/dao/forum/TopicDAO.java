package net.darkforum.dao.forum;

import net.darkforum.dao.BasicDAO;
import net.darkforum.dto.forum.TopicFilterDTO;
import net.darkforum.entity.forum.Topic;

import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 */
public class TopicDAO extends BasicDAO<Topic> {

    public List<Topic> listByFilter(TopicFilterDTO filter) {

        Map<String, Object> params = new HashMap<>();
        TypedQuery<Topic> query = getEntityManager().createQuery(
                createQueryByFilter(filter, params, false),
                Topic.class
        );
        params.forEach(query::setParameter);

        if (Objects.nonNull(filter.getFirst())) {
            query.setFirstResult(filter.getFirst());
        }
        if (Objects.nonNull(filter.getRows())) {
            query.setMaxResults(filter.getRows());
        }

        return query.getResultList();
    }

    public Long totalByFilter(TopicFilterDTO filter) {
        Map<String, Object> params = new HashMap<>();
        TypedQuery<Long> query = getEntityManager().createQuery(
                createQueryByFilter(filter, params, true),
                Long.class
        );
        params.forEach(query::setParameter);
        return query.getSingleResult();
    }

    private String createQueryByFilter(TopicFilterDTO filter, Map<String, Object> params, boolean count) {
        StringBuilder sql = new StringBuilder();
        if( count ) {
            sql.append(" SELECT count(t.id) FROM Topic t ");
        } else {
            sql.append(" SELECT t FROM Topic t ");
        }

        sql.append(" WHERE t.deleteDate IS NULL ");
        if (Objects.nonNull(filter.getIdForum())) {
            sql.append(" AND t.forum.id = :idForum ");
            params.put("idForum", filter.getIdForum());
        }
        if (Boolean.TRUE.equals(filter.getHot())) {
            sql.append(" AND t.views > 0 ");
        }
        sql.append(" ORDER BY ");
        if (Boolean.TRUE.equals(filter.getHot())) {
            sql.append(" t.views DESC, t.createDate DESC ");
        } else {
            sql.append(" t.locked DESC, t.createDate DESC ");
        }
        return sql.toString();
    }

    public Topic findBySlug(String slug) {
        return getEntityManager().createQuery(
                " SELECT t FROM Topic t WHERE t.slug = :slug ",
                Topic.class
        ).setParameter("slug", slug).getSingleResult();
    }

}
