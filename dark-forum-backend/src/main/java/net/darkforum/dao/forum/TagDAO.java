package net.darkforum.dao.forum;

import net.darkforum.dao.BasicDAO;
import net.darkforum.entity.forum.Tag;

/**
 *
 */
public class TagDAO extends BasicDAO<Tag> {

    public Tag findByName(String name) {
        return getEntityManager().createQuery(
                "select t from Tag t where t.name = :name",
                Tag.class
        ).setParameter(
                "name", name
        ).getSingleResult();
    }

}
