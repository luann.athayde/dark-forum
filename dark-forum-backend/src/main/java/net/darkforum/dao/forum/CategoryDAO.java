package net.darkforum.dao.forum;

import net.darkforum.dao.BasicDAO;
import net.darkforum.entity.forum.Category;
import net.darkforum.entity.forum.Group;

/**
 *
 */
public class CategoryDAO extends BasicDAO<Category> {

    public Category findBySlug(String slug) {
        return getEntityManager().createQuery(
                "SELECT c FROM Category c WHERE c.slug = :slug AND c.deleteDate IS NULL " +
                        " ORDER BY c.pinned DESC, c.id DESC",
                Category.class
        ).setParameter("slug", slug)
                .getSingleResult();
    }

}
