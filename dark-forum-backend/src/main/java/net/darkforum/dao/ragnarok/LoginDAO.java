package net.darkforum.dao.ragnarok;

import net.darkforum.dao.BasicDAO;
import net.darkforum.entity.ragnarok.Login;

/**
 *
 */
public class LoginDAO extends BasicDAO<Login> {

    public Login findByUserAndPassword(String userid, String password) {
        return getRagnarokEntityManager().createQuery(
                " select login from Login login WHERE login.userid = :userid and login.password = :password ",
                Login.class
        ).setParameter("userid", userid)
                .setParameter("password", password)
                .getSingleResult();
    }

}
