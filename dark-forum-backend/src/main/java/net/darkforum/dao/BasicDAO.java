package net.darkforum.dao;

import net.darkforum.dto.FilterDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * @author Luann Athayde
 * @version 1.0.0
 */
public class BasicDAO<E> implements Serializable {

    protected Class entityClass;

    @PersistenceContext(unitName = "dark-forum")
    protected EntityManager entityManager;

    @PersistenceContext(unitName = "ragnarok")
    protected EntityManager ragnarokEntityManager;

    public BasicDAO() throws IllegalStateException {
        try {
            ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
            this.entityClass = (Class) type.getActualTypeArguments()[0];
        } catch (Exception e) {
            throw new IllegalStateException("Fail to get class from typed param!");
        }
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    protected EntityManager getRagnarokEntityManager() {
        return ragnarokEntityManager;
    }

    public E save(E entity) {
        return getEntityManager().merge(entity);
    }

    public E findByPk(Object pk) {
        return (E) getEntityManager().find(entityClass, pk);
    }

    @SuppressWarnings("all")
    public List<E> listAll(FilterDTO filter) {
        return getEntityManager().createQuery(
                "SELECT e FROM " + entityClass.getSimpleName() + " e WHERE e.deleteDate IS NULL ",
                entityClass
        ).setMaxResults(
                filter.getRows()
        ).setFirstResult(
                filter.getFirst()
        ).getResultList();
    }

}
