package net.darkforum.parser;

public abstract class Parser<ENTITY, DTO> {

    public abstract ENTITY toEntity(DTO dto);
    public abstract DTO toDto(ENTITY entity);

}
