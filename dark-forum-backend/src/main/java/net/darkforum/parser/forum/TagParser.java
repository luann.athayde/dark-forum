package net.darkforum.parser.forum;

import net.darkforum.dto.forum.TagDTO;
import net.darkforum.entity.forum.Tag;
import net.darkforum.parser.Parser;
import org.modelmapper.ModelMapper;

import java.util.Objects;

public class TagParser extends Parser<Tag, TagDTO> {

    private static TagParser instance = new TagParser();

    public static TagParser get() {
        return instance;
    }

    @Override
    public Tag toEntity(TagDTO dto) {

        ModelMapper mapper = new ModelMapper();

        Tag domain = mapper.map(dto, Tag.class);

        return domain;
    }

    @Override
    public TagDTO toDto(Tag domain) {

        ModelMapper mapper = new ModelMapper();

        TagDTO dto = mapper.map(domain, TagDTO.class);

        if (Objects.nonNull(domain.getUser())) {
            dto.setUser(UserParser.get().toDto(domain.getUser()));
        }

        return dto;
    }

}
