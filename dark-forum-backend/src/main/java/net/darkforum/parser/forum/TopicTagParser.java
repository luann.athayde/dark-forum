package net.darkforum.parser.forum;

import net.darkforum.dto.forum.TopicTagDTO;
import net.darkforum.entity.forum.TopicTag;
import net.darkforum.parser.Parser;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.Objects;

public class TopicTagParser extends Parser<TopicTag, TopicTagDTO> {

    private static TopicTagParser instance = new TopicTagParser();

    public static TopicTagParser get() {
        return instance;
    }

    @Override
    public TopicTag toEntity(TopicTagDTO dto) {

        ModelMapper mapper = new ModelMapper();

        TopicTag domain = mapper.map(dto, TopicTag.class);
        domain.setCreateDate( LocalDateTime.now() );

        return domain;
    }

    @Override
    public TopicTagDTO toDto(TopicTag domain) {

        ModelMapper mapper = new ModelMapper();

        TopicTagDTO dto = mapper.map(domain, TopicTagDTO.class);

        if (Objects.nonNull(domain.getUser())) {
            dto.setUser(UserParser.get().toDto(domain.getUser()));
        }

        return dto;
    }

}
