package net.darkforum.parser.forum;

import lombok.extern.slf4j.Slf4j;
import net.darkforum.dto.forum.UserDTO;
import net.darkforum.entity.forum.User;
import net.darkforum.parser.Parser;
import net.darkforum.util.MediaUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;

import java.io.File;

@Slf4j
public class UserParser extends Parser<User, UserDTO> {

    private static boolean LOADED_IMAGE = false;
    private static String DEFAULT_THUMB_IMAGE = StringUtils.EMPTY;

    public UserParser() {
        if (Boolean.FALSE.equals(LOADED_IMAGE)) {
            try (var in = getClass().getResourceAsStream("/images/thumb.png")) {
                DEFAULT_THUMB_IMAGE = MediaUtils.convertFileToBase64(in, in.available());
            } catch (Exception e) {
                log.warn("### Failed to load default THUMB image!", e);
            } finally {
                LOADED_IMAGE = true;
            }
        }
    }

    public static UserParser get() {
        return new UserParser();
    }

    @Override
    public User toEntity(UserDTO dto) {

        var mapper = new ModelMapper();

        var domain = mapper.map(dto, User.class);
        domain.setOnline(Boolean.FALSE.equals(dto.getOnline()));

        return domain;
    }

    @Override
    public UserDTO toDto(User domain) {

        var mapper = new ModelMapper();
        var dto = mapper.map(domain, UserDTO.class);

        if (StringUtils.isNotEmpty(domain.getProfilePicture())) {
            var profilePictureFile = new File(domain.getProfilePicture());
            if (profilePictureFile.exists() && profilePictureFile.canRead()) {
                dto.setProfilePicture(MediaUtils.convertFileToBase64(profilePictureFile));
            }
        } else {
            dto.setProfilePicture(DEFAULT_THUMB_IMAGE);
        }

        return dto;
    }

}
