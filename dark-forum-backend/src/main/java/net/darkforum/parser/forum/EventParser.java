package net.darkforum.parser.forum;

import net.darkforum.dto.forum.EventDTO;
import net.darkforum.entity.forum.Event;
import net.darkforum.parser.Parser;
import org.modelmapper.ModelMapper;

import java.util.Objects;

public class EventParser extends Parser<Event, EventDTO> {

    private static EventParser instance = new EventParser();

    public static EventParser get() {
        return instance;
    }

    @Override
    public Event toEntity(EventDTO dto) {

        ModelMapper mapper = new ModelMapper();

        Event domain = mapper.map(dto, Event.class);

        return domain;
    }

    @Override
    public EventDTO toDto(Event domain) {

        ModelMapper mapper = new ModelMapper();

        EventDTO dto = mapper.map(domain, EventDTO.class);

        if (Objects.nonNull(domain.getUser())) {
            dto.setUser(UserParser.get().toDto(domain.getUser()));
        }

        return dto;
    }

}
