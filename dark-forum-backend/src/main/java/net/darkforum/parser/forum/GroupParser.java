package net.darkforum.parser.forum;

import net.darkforum.dto.forum.GroupDTO;
import net.darkforum.entity.forum.Group;
import net.darkforum.parser.Parser;
import org.modelmapper.ModelMapper;

import java.util.Objects;

public class GroupParser extends Parser<Group, GroupDTO> {

    private static GroupParser instance = new GroupParser();

    public static GroupParser get() {
        return instance;
    }

    @Override
    public Group toEntity(GroupDTO dto) {

        ModelMapper mapper = new ModelMapper();

        Group domain = mapper.map(dto, Group.class);

        return domain;
    }

    @Override
    public GroupDTO toDto(Group domain) {

        ModelMapper mapper = new ModelMapper();

        GroupDTO dto = mapper.map(domain, GroupDTO.class);

        return dto;
    }

}
