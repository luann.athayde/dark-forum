package net.darkforum.parser.forum;

import net.darkforum.dto.forum.TopicDTO;
import net.darkforum.entity.forum.Topic;
import net.darkforum.parser.Parser;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.Objects;

public class TopicParser extends Parser<Topic, TopicDTO> {

    private static TopicParser instance = new TopicParser();

    public static TopicParser get() {
        return instance;
    }

    @Override
    public Topic toEntity(TopicDTO dto) {

        ModelMapper mapper = new ModelMapper();

        Topic domain = mapper.map(dto, Topic.class);

        if (Objects.isNull(dto.getLocked())) {
            domain.setLocked(Boolean.FALSE);
        }
        if (Objects.isNull(dto.getCreateDate())) {
            domain.setCreateDate(LocalDateTime.now());
        }
        if (Objects.isNull(dto.getViews())) {
            domain.setViews(0L);
        }

        return domain;
    }

    @Override
    public TopicDTO toDto(Topic domain) {

        ModelMapper mapper = new ModelMapper();

        TopicDTO dto = mapper.map(domain, TopicDTO.class);
        dto.getTags().forEach( topicTagDTO -> topicTagDTO.setTopic(null) );

        if (Objects.nonNull(domain.getUser())) {
            dto.setUser(UserParser.get().toDto(domain.getUser()));
        }

        return dto;
    }

}
