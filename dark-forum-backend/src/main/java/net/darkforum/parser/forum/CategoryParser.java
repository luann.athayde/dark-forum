package net.darkforum.parser.forum;

import net.darkforum.dto.forum.CategoryDTO;
import net.darkforum.entity.forum.Category;
import net.darkforum.parser.Parser;
import org.modelmapper.ModelMapper;

import java.util.Objects;

public class CategoryParser extends Parser<Category, CategoryDTO> {

    private static CategoryParser instance = new CategoryParser();

    public static CategoryParser get() {
        return instance;
    }

    @Override
    public Category toEntity(CategoryDTO dto) {

        ModelMapper mapper = new ModelMapper();

        Category domain = mapper.map(dto, Category.class);

        return domain;
    }

    @Override
    public CategoryDTO toDto(Category domain) {

        ModelMapper mapper = new ModelMapper();

        CategoryDTO dto = mapper.map(domain, CategoryDTO.class);

        if (Objects.nonNull(domain.getUser())) {
            dto.setUser(UserParser.get().toDto(domain.getUser()));
        }

        return dto;
    }

}
