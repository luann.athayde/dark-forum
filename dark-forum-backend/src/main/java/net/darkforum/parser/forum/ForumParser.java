package net.darkforum.parser.forum;

import net.darkforum.dto.forum.ForumDTO;
import net.darkforum.entity.forum.Forum;
import net.darkforum.parser.Parser;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.Objects;

public class ForumParser extends Parser<Forum, ForumDTO> {

    private static ForumParser instance = new ForumParser();

    public static ForumParser get() {
        return instance;
    }

    @Override
    public Forum toEntity(ForumDTO dto) {

        ModelMapper mapper = new ModelMapper();

        Forum domain = mapper.map(dto, Forum.class);

        if( Objects.isNull(dto.getTotalTopics()) ) {
            domain.setTotalTopics(0L);
        }
        if (Objects.isNull(dto.getCreateDate())) {
            domain.setCreateDate(LocalDateTime.now());
        }

        domain.setPinned(Boolean.TRUE.equals(dto.getPinned()));
        domain.setLocked(Boolean.TRUE.equals(dto.getLocked()));

        return domain;
    }

    @Override
    public ForumDTO toDto(Forum domain) {

        ModelMapper mapper = new ModelMapper();

        ForumDTO dto = mapper.map(domain, ForumDTO.class);

        if (Objects.nonNull(domain.getUser())) {
            dto.setUser(UserParser.get().toDto(domain.getUser()));
        }

        return dto;
    }

}
