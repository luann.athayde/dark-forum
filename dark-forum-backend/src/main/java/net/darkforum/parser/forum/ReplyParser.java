package net.darkforum.parser.forum;

import net.darkforum.dto.forum.ReplyDTO;
import net.darkforum.entity.forum.Reply;
import net.darkforum.parser.Parser;
import org.modelmapper.ModelMapper;

import java.util.Objects;

public class ReplyParser extends Parser<Reply, ReplyDTO> {

    private static ReplyParser instance = new ReplyParser();

    public static ReplyParser get() {
        return instance;
    }

    @Override
    public Reply toEntity(ReplyDTO dto) {

        ModelMapper mapper = new ModelMapper();

        Reply domain = mapper.map(dto, Reply.class);

        return domain;
    }

    @Override
    public ReplyDTO toDto(Reply domain) {

        ModelMapper mapper = new ModelMapper();

        ReplyDTO dto = mapper.map(domain, ReplyDTO.class);

        if (Objects.nonNull(domain.getUser())) {
            dto.setUser(UserParser.get().toDto(domain.getUser()));
        }

        return dto;
    }

}
