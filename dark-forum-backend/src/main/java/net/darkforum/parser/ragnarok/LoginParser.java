package net.darkforum.parser.ragnarok;

import net.darkforum.dto.ragnarok.LoginDTO;
import net.darkforum.entity.ragnarok.Login;
import net.darkforum.parser.Parser;
import net.darkforum.util.DateUtils;
import org.modelmapper.ModelMapper;

public class LoginParser extends Parser<Login, LoginDTO> {

    private static LoginParser instance = new LoginParser();

    public static LoginParser get() {
        return instance;
    }

    @Override
    public Login toEntity(LoginDTO dto) {

        ModelMapper mapper = new ModelMapper();

        Login domain = mapper.map(dto, Login.class);

        return domain;
    }

    @Override
    public LoginDTO toDto(Login domain) {

        ModelMapper mapper = new ModelMapper();

        LoginDTO dto = mapper.map(domain, LoginDTO.class);

        return dto;
    }

}
