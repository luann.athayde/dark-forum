package net.darkforum.dto;

import lombok.Data;

import javax.ws.rs.QueryParam;
import java.io.Serializable;

@Data
public class FilterDTO implements Serializable {

    @QueryParam("first")
    private Integer first;
    @QueryParam("rows")
    private Integer rows;
    @QueryParam("sortField")
    private String sortField;
    @QueryParam("sortOrder")
    private String sortOrder;

}
