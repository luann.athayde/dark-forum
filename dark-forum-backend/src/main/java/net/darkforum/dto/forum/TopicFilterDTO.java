package net.darkforum.dto.forum;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.darkforum.dto.FilterDTO;

import javax.ws.rs.QueryParam;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TopicFilterDTO extends FilterDTO {

    @QueryParam("id")
    private Long id;

    @QueryParam("idForum")
    private Long idForum;

    @QueryParam("idTopic")
    private Long idTopic;

    @QueryParam("name")
    private String name;

    @QueryParam("hot")
    private Boolean hot;

    @QueryParam("slug")
    private String slug;

    @QueryParam("idUser")
    private Long idUser;

}
