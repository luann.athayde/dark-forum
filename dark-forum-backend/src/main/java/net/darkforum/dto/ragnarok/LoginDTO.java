/**
 *
 */
package net.darkforum.dto.ragnarok;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.darkforum.util.LocalDateDeserializer;
import net.darkforum.util.LocalDateSerializer;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author Luann Athayde
 * @version 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginDTO implements Serializable {

    private Long accountId;

    private String userid;
    private String email;
    private String sex;
    private String password;
    private String passwordConfirmation;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate birthdate;

    private String lastLogin;
    private String lastIp;
    private String macAdress;
    private String lastPincodeChange;
    private String currentIp;

    private Long groupId;

    private String pincode;
    private String pincodeChange;
    private String pincodeChangeISO;

    private String register;
    private String login;

}
