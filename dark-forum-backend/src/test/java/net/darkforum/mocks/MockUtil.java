package net.darkforum.mocks;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class MockUtil {

    public static <T> T loadMock(Class<T> clazz, String json) throws IOException {
        return new ObjectMapper().readValue(
                ClassLoader.getSystemResourceAsStream(json),
                clazz
        );
    }

}
