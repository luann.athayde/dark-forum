package net.darkforum.service;


import net.darkforum.constants.Configuration;
import net.darkforum.dao.forum.GroupDAO;
import net.darkforum.dao.forum.UserDAO;
import net.darkforum.dao.ragnarok.LoginDAO;
import net.darkforum.dto.forum.UserDTO;
import net.darkforum.dto.ragnarok.LoginDTO;
import net.darkforum.entity.forum.User;
import net.darkforum.entity.ragnarok.Login;
import net.darkforum.exception.ValidationException;
import net.darkforum.mocks.MockUtil;
import net.darkforum.parser.forum.UserParser;
import net.darkforum.parser.ragnarok.LoginParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    protected UserService userService;

    @Mock
    protected GroupDAO groupDAO;
    @Mock
    protected UserDAO userDAO;
    @Mock
    protected LoginDAO loginDAO;
    @Mock
    protected UserService self;

    protected UserDTO userDTO;
    protected User user;
    protected LoginDTO loginDTO;
    protected Login login;

    @Before
    public void setup() throws IOException {
        System.setProperty(Configuration.RAGNAROK_LOGIN_INTEGRATION.getPropertyName(), "false");

        userDTO = MockUtil.loadMock(UserDTO.class, "user_full.json");
        user = UserParser.get().toEntity(userDTO);

        loginDTO = MockUtil.loadMock(LoginDTO.class, "login_full.json");
        login = LoginParser.get().toEntity(loginDTO);

    }

    @Test
    public void mustSucceedWhenFindByUserAndPassword() throws ValidationException {
        System.setProperty(Configuration.RAGNAROK_LOGIN_INTEGRATION.getPropertyName(), "false");

        Mockito.when(
                userDAO.findByUserAndPassword(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())
        ).thenReturn(user);

        var foundedUser = userService.findByUserAndPassword(userDTO.getUserid(), userDTO.getPassword());
        Assert.assertNotNull(foundedUser);

    }

    @Test
    public void mustSucceedWhenFindByUserAndPasswordAsRagnarokAccount() throws ValidationException {
        System.setProperty(Configuration.RAGNAROK_LOGIN_INTEGRATION.getPropertyName(), "true");

        Mockito.when(
                loginDAO.findByUserAndPassword(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())
        ).thenReturn(login);
        Mockito.when(
                userDAO.findByEmail(ArgumentMatchers.anyString())
        ).thenReturn(user);

        var foundedUser = userService.findByUserAndPassword(userDTO.getUserid(), userDTO.getPassword());
        Assert.assertNotNull(foundedUser);

    }

    @Test
    public void mustSucceedWhenCreateUserByRagnarokAccount() {

        Mockito.when(
                groupDAO.findByPk(ArgumentMatchers.anyLong())
        ).thenReturn(user.getGroup());
        Mockito.when(
                userDAO.save(ArgumentMatchers.any(User.class))
        ).thenReturn(user);

        var createdUser = userService.createUserByRagnarokAccount(loginDTO);
        Assert.assertNotNull(createdUser);

    }

}
